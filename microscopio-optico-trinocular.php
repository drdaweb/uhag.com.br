
      <?php
      include('inc/vetKey.php');
      $h1             = "Microscópio óptico trinocular";
      $title          = $h1;
      $desc           = "E se tratando de microscópio óptico trinocular, a empresa que melhor poderá te atender é a UHAG. A empresa está no mercado desde 1927 São muitos anos de";
      $key            = "microscopio,optico,trinocular";
      $legendaImagem  = "Foto ilustrativa de Microscópio óptico trinocular";
      $pagInterna     = "Informações";
      $urlPagInterna  = "informacoes";
      include('inc/head.php');
      include('inc/fancy.php');
      ?>
      <script defer src="<?=$url?>js/organictabs.jquery.js" ></script>
    </head>
    <body>
      
      <? include('inc/topo.php');?>
      <div class="wrapper">
        <main>
          <div class="content" itemscope itemtype="https://schema.org/Product">
            <section>
              <?=$caminho2?>
              <h1><?=$h1?></h1>
              <article>
                <? $quantia = 4; include('inc/gallery.php');?>
                <p class="alerta">Clique nas imagens para ampliar</p>
                <h2>CONHECENDO A UTILIZAÇÃO DE UM MICROSCÓPIO ÓPTICO TRINOCULAR</h2>

<p>Um laboratório é um espaço físico que possui diversos tipos de instrumentos que poderão ser utilizados para realizar experimentos, pesquisas científicas e estudos. Hoje em dia, existem diversos tipos de laboratórios, cada um deles será especializado em uma determinada área. Podemos começar falando dos laboratórios de microbiologia e parasitologia. Esses laboratórios são responsáveis por fazer a análise de microrganismos, bactérias, vírus e parasitas prejudiciais para o homem. Existem também os laboratórios toxicológicos que irão realizar análises toxicológicas em matrizes biológicas de seres humanos, plantas, peixes, entre outros. E o que todos esses laboratórios têm em comum são os microscópios. Os microscópios são peças fundamentais para o desenvolvimento correto das pesquisas realizadas nesses laboratórios. Além disso, podem existir microscópios certos para cada utilidade. Por exemplo, o <strong>microscópio óptico trinocular</strong>. O <strong>microscópio óptico trinocular</strong> é um equipamento que, como o próprio nome sugere, possui três lentes e possui um design moderno e arrojado. O <strong>microscópio óptico trinocular</strong> possui alto nível de tecnologia e modernidade e isso é um dos motivos que fazem o equipamento ser utilizado em larga escala em diversos ramos industriais.</p>

<h2>TIPOS DE MICROSCÓPIO ÓPTICO TRINOCULAR</h2>

<p>Existem diversos tipos de <strong>microscópio óptico trinocular</strong> disponíveis no mercado. Cada um deles é indicado para um determinado tipo de uso. Os modelos mais utilizados são:</p>

<ul class="list">
  <li><b> microscópio óptico trinocular biológico:</b> Utilizado em grande escala em laboratórios clínicos, biológicos e para realizar análises didáticas em geral;</li>  
  <li> <b>Microscópio trinocular de Imunofluorescência:</b> utilizado na detecção de DSTs e também no diagnóstico de deficiências imunológicas. É acoplado com iluminador de EPI fluorescência;</li>  
  <li> <b>Microscópio trinocular biológico invertido:</b> Esse modelo permite a melhor visualização das substâncias. A imagem é nítida e com alta resolução. É indicado para processos de cultura celular, biologia celular e microbiologia.</li></ul>

<h2>PROCURANDO EMPRESAS PARA ADQUIRIR UM MICROSCÓPIO TRINOCULAR</h2>

<p>E se queremos produtos de qualidade, é fundamental encontrar empresas com experiência no ramo. E se tratando de <strong>microscópio óptico trinocular</strong>, a empresa que melhor poderá te atender é a UHAG. A empresa está no mercado desde 1927 São muitos anos de experiência em desenvolvimento de equipamentos. Contate a empresa, conheça os produtos e serviços disponíveis e se surpreenda com a qualidade oferecida.</p>




                <? include('inc/saiba-mais.php');?>
                <? include('inc/social-media.php');?>
              </article>
              <? include('inc/coluna-lateral.php');?>
              <br class="clear" />
              <? include('inc/paginas-relacionadas.php');?>
              <? include('inc/regioes-brasil.php');?>
              <br class="clear">
              <? include('inc/copyright.php');?>
            </section>
          </div>
        </main>
      </div><!-- .wrapper -->
      <? include('inc/footer.php');?>
    </body>
    </html>