
      <?php
      include('inc/vetKey.php');
      $h1             = "Durômetro de bancada";
      $title          = $h1;
      $desc           = "O durômetro de bancada é utilizado principalmente por empresas industriais que necessitam aferir a dureza de suas peças antes de serem completamente produzidas.";
      $key            = "durometro,bancada";
      $legendaImagem  = "Foto ilustrativa de Durômetro de bancada";
      $pagInterna     = "Informações";
      $urlPagInterna  = "informacoes";
      include('inc/head.php');
      include('inc/fancy.php');
      ?>
      <script defer src="<?=$url?>js/organictabs.jquery.js" ></script>
    </head>
    <body>
      
      <? include('inc/topo.php');?>
      <div class="wrapper">
        <main>
          <div class="content" itemscope itemtype="https://schema.org/Product">
            <section>
              <?=$caminho2?>
              <h1><?=$h1?></h1>
              <article>
                <? $quantia = 5; include('inc/gallery.php');?>
                <p class="alerta">Clique nas imagens para ampliar</p>
                <h2>DURÔMETRO DE BANCADA. ALTA EFICIÊNCIA EM CONTROLE DE DUREZA</h2>

<p>O <strong>durômetro de bancada</strong> é utilizado principalmente por empresas industriais que necessitam aferir a dureza de suas peças antes de serem completamente produzidas. Desta maneira, o equipamento é considerado um controlador de qualidade, uma vez que detecta as inconformidades das peças a serem fabricadas.</p>

<h2>PRINCIPAIS VANTAGENS EM UTILIZAR O DURÔMETRO DE BANCADA</h2>

<p>O <strong>durômetro de bancada</strong> é usado em peças de pequeno porte; por isso, sua estrutura é compacta e leve, o que favorece a movimentação e a instalação do equipamento, que não necessita de grandes espaços para ser alocado. Além disso, o durômetro é um equipamento que oferece segurança, pois seu manejo é muito facilitado. </p>

<p>A utilização do <strong>durômetro de bancada</strong> proporciona grandes ganhos em qualidade e produtividade, pois realiza a análise de vários pontos da peça, tais como: resistência a impactos, solda, altas temperaturas, entre outros fatores importantes  relacionados ao controle de qualidade.</p>

<p>O equipamento é constituído de materiais nobres e dispositivos tecnológicos que favorecem a sua longa durabilidade. Ademais, o <strong>durômetro de bancada</strong> pode ser aplicado em laboratórios, durante a inspeção, na ferramentaria, no chão de fábrica, entre outros ambientes industriais.</p>

<p>Há no mercado três tipos deste equipamento, diferenciados pelo tipo de escala e penetrador (diamante ou esfera). São eles:</p>

<ul class="list">
  <li><b>Durômetro modelo Rockwell:</b> pode ser encontrado nos modelos analógicos e digitais. Realiza ensaios em variados tipos de peças metálicas. Possui estrutura rígida e sensível para leitura precisa;</li>
  
  <li><b>Durômetro modelo Brinel (HB):</b> fabricado em ferro fundido, faz a seleção de cargas manualmente ou de forma automática. Possui dispositivo de impacto tipo D e capacidade de 230 mm vertical e 120 mm horizontal;</li>
  
  <li><b>Durômetro modelo Vickers (HV):</b> também fabricado com ferro fundido, verifica a dureza de micropeças. Integra tecnologias mecânicas, fotoelétricas, entre outras.</li>
</ul>

<h2>TODOS OS MODELOS DE DURÔMETRO DE BANCADA VOCÊ ENCONTRA NA UHAG!</h2>

<p>A UHAG possui todos os modelos de durômetro de bancada, tais como Rockwell (HR), Brinell (HB) e Vickers (HV). Com total segurança de qualidade, pois UHAG somente atua com as marcas importantes do mundo. Por isso, não perca tempo e venha conhecer a nossa linha completa de produtos para o seu negócio!</p>








                <? include('inc/saiba-mais.php');?>
                <? include('inc/social-media.php');?>
              </article>
              <? include('inc/coluna-lateral.php');?>
              <br class="clear" />
              <? include('inc/paginas-relacionadas.php');?>
              <? include('inc/regioes-brasil.php');?>
              <br class="clear">
              <? include('inc/copyright.php');?>
            </section>
          </div>
        </main>
      </div><!-- .wrapper -->
      <? include('inc/footer.php');?>
    </body>
    </html>