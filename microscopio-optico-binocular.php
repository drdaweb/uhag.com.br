
      <?php
      include('inc/vetKey.php');
      $h1             = "Microscópio óptico binocular";
      $title          = $h1;
      $desc           = "o microscópio óptico binocular atua diretamente com um sistema de oculares (e também por isso sua nomenclatura se dá desta maneira). Não bastasse, o conceito";
      $key            = "microscopio,optico,binocular";
      $legendaImagem  = "Foto ilustrativa de Microscópio óptico binocular";
      $pagInterna     = "Informações";
      $urlPagInterna  = "informacoes";
      include('inc/head.php');
      include('inc/fancy.php');
      ?>
      <script defer src="<?=$url?>js/organictabs.jquery.js" ></script>
    </head>
    <body>
      
      <? include('inc/topo.php');?>
      <div class="wrapper">
        <main>
          <div class="content" itemscope itemtype="https://schema.org/Product">
            <section>
              <?=$caminho2?>
              <h1><?=$h1?></h1>
              <article>
                <? $quantia = 9; include('inc/gallery.php');?>
                <p class="alerta">Clique nas imagens para ampliar</p>
                <h2>O MICROSCÓPIO ÓPTICO BINOCULAR É MUITO UTILIZADO EM LABORATÓRIOS BIOLÓGICOS</h2>

<p>Por mais que também possa marcar presença em outros ambientes comerciais e/ou industriais, o <strong>microscópio óptico binocular</strong> é de essencial aplicação nos laboratórios biológicos. Trata-se, portanto, de uma peça que auxilia a visualização de áreas de inspeção e verificação, uma vez que oferece uma ampliação cinco vezes maior do que o que é observado nos demais dispositivos deste mercado. Além disso, o <strong>microscópio óptico binocular</strong> pode chegar a níveis de ampliação que beirem até mil vezes em comparação ao que é notado na óptica real.</p>

<p>Tecnicamente, o <strong>microscópio óptico binocular</strong> atua diretamente com um sistema de oculares (e também por isso sua nomenclatura se dá desta maneira). Não bastasse, o conceito de binocular se integra total e completamente às duas vezes em que o equipamento contribui na ampliação de observação quando da profundidade real da peça trabalhada. O intuito da utilização do equipamento é fazer com estas análises não fiquem desfocadas por qualquer razão interna e inerente ao aparelho ou até mesmo prejudicadas por alguma razão externa do espaço laboratorial ou industrial. Ao não promover qualquer tipo de medição real nestas análises, o dispositivo é útil somente para os processos de inspeção a que plásticos e outras estruturas são submetidas no dia a dia produtivo de uma indústria.</p>

<h2>O MICROSCÓPIO ÓPTICO BINOCULAR É PERFEITO NA PERCEPÇÃO DE REBARBAS</h2>

<p>Uma vez que amplia consideravelmente a possibilidade de uma análise profissional fria e completa quando da formatação de um material observado, o <strong>microscópio óptico binocular</strong> é perfeito para que as eventuais percepções de rebarbas que possam vir a assolar estes produtos sejam devidamente solucionadas a níveis estruturais. Isto é, para que o controle do acabamento final das peças seja o mais otimizado possível, esta visualização completa é imprescindível, pois somente desta forma é que se faz possível alterar – para melhor – a composição física de um material analisado no microscópio.</p>

<h2>MICROSCÓPIO ÓPTICO BINOCULAR É NA UHAG</h2>

<p>A UHAG é uma empresa com mais de 90 anos de experiência ligados ao setor industrial e fica situada em Vinhedo, no interior paulista. As principais palavras-chave que mais norteiam as condutas da instituição são seriedade, inovação e comprometimento.</p>



                <? include('inc/saiba-mais.php');?>
                <? include('inc/social-media.php');?>
              </article>
              <? include('inc/coluna-lateral.php');?>
              <br class="clear" />
              <? include('inc/paginas-relacionadas.php');?>
              <? include('inc/regioes-brasil.php');?>
              <br class="clear">
              <? include('inc/copyright.php');?>
            </section>
          </div>
        </main>
      </div><!-- .wrapper -->
      <? include('inc/footer.php');?>
    </body>
    </html>