<?php
if (!$Read):
  $Read = new Read;
endif;

/**
 * <b>Montagem do breadcrumb</b>
 * Pegar urls amigaveis e titulo dessas urls
 */
//Variavel que vai receber os itens filhos (categoria)
$arrBreadcrump = array();

//Get dados da sessão pai
if (!empty($URL[0])):
  $Read->ExeRead(TB_PAGINA, "WHERE pag_name = :nm AND pag_status = :status AND user_empresa = :emp", "nm={$URL[0]}&status=2&emp=" . EMPRESA_CLIENTE);
  $itemSessao = $Read->getResult();
  $arrBreadcrump[] = array('titulo' => $itemSessao[0]['pag_title'], 'url' => $itemSessao[0]['pag_name'], 'parent' => $itemName[0]['cat_parent']);
  extract($itemSessao[0]);
endif;

include('inc/head.php');
include('inc/fancy.php');
?>

</head>
<body>
  <?php include('inc/topo.php'); ?>
  <div class="wrapper">

    <main role="main">
      <div class="content">
        <section>
          <!-- Breadcrump -->
          <?php Check::SetBreadcrumb($arrBreadcrump); ?> 

          <h1 class="title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?></h1>   

          <div class="htmlchars">                    
            <?= $pag_content; ?>
          </div>

          <div class="clear"></div>
          <?php include('inc/social-media.php'); ?>
        </section>  
      </div>
    </main>    

  </div>

  <?php include('inc/footer.php'); ?>

</body>
</html>