<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 1;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-database"></i> Lista de candidatos</h3>
    </div>
    <div class="clearfix"></div>
    <br/>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">  
        <div class="x_panel"> 
          <div class="x_content">
            <?php
            $ReadCand = new Read;
            $ReadCand->ExeRead(TB_CANDIDATO, "WHERE user_empresa = :emp ORDER BY cand_date DESC", "emp={$_SESSION['userlogin']['user_empresa']}");
            if (!$ReadCand->getResult()):
              WSErro("Nenhum candidato foi encontrado.", WS_INFOR, null, "Aviso!");
            else:
              ?>
              <div class="x_title">
                <h2>Confira a lista de candidatos que enviaram currículos.<small>Você também pode remover o candidato ou fazer o download do currículo.</small></h2>                           
                <div class="clearfix"></div>                            
              </div>
              <br/>                                                     
              <div class="table-responsive">
                <table id="datatable" class="table table-striped dtr-inline dataTable jambo_table nowrap">
                  <thead>
                    <tr>
                      <th>Ações</th>                                     
                      <th>Currículo</th>                                
                      <th>Nome</th>                                
                      <th>E-mail</th>                                                                       
                      <th>Telefone</th>
                      <th>Vaga</th>
                      <th>Estágio?</th>                                
                      <th>Cargo</th>                               
                      <th>Data</th>                               
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($ReadCand->getResult() as $dados):
                      extract($dados);
                      ?>
                      <tr class="j_item" id="<?= $cand_id; ?>">
                        <td style="width: 70px;">
                          <div class="btn-group btn-group-xs">                                                                                                                                                                                                            
                            <button type="button" class="btn btn-danger j_remove" rel="<?= $cand_id; ?>" action="RemoveCandidato"><i class="fa fa-trash"></i></button>                                                    
                          </div>                                                        
                        </td>                                            
                        <td>
                          <div class="btn-group btn-group-xs">  
                            <a href="<?= BASE . '/uploads/' . $cand_file; ?>" title="Currículo de <?= $cand_nome; ?>" class="btn btn-info" target="_blank"><i class="fa fa-download"></i> Visualizar</a>
                          </div>
                        </td>                                   
                        <td><?= $cand_nome; ?></td>                                   
                        <td><?= $cand_email; ?></td>
                        <td><?= $cand_telefone; ?></td>
                        <td><?= Check::VagaById($cand_vaga); ?></td>
                        <td><?= getQuestion($cand_estagio); ?></td>
                        <td><?= $cand_cargo; ?></td>                                   
                        <td><?= date("d/m/Y H:i", strtotime($cand_date)); ?></td>                                            
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>                        
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>