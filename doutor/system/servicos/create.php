<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 1;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">

    <div class="page-title">
      <div class="title col-md-12 col-sm-6 col-xs-12">
        <h3><i class="fa fa-cogs"></i> Cadastro de serviços</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="pull-right">
            <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=servicos/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br>
          <?php
          $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['Cadastra'])):
            unset($post['Cadastra'], $post['buscaFile']);
            $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
            $post['serv_cover'] = ( $_FILES['serv_cover']['tmp_name'] ? $_FILES['serv_cover'] : null );
            $post['gallery_file'] = ( $_FILES['gallery_file']['tmp_name'] ? $_FILES['gallery_file'] : null );

            if (empty($post['url_relation'])):
              $post['url_relation'] = "0";
            else:
              foreach ($post['url_relation'] as $urlrelat):
                $urlr .= $urlrelat . ',';
              endforeach;
              $post['url_relation'] = $urlr;
            endif;

            $cadastra = new Servicos();
            $cadastra->ExeCreate($post);

            if (!$cadastra->getResult()):
              $erro = $cadastra->getError();
              WSErro($erro[0], $erro[1], null, $erro[2]);
            else:
              $_SESSION['Error'] = $cadastra->getError();
              header('Location: painel.php?exe=servicos/create&get=true');
            endif;

            var_dump($cadastra);

          endif;

          if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
            //COLOCAR ALERTA PERSONALIZADOS
            WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
            unset($_SESSION['Error']);
          endif;
          ?>                    
          <div class="x_panel">
                          <div class="x_title">
                <h2>  Abaixo você você pode cadastrar os serviços que vão aparecer em seu site.</h2>                           
                <div class="clearfix"></div>                            
              </div>
<div class="x_content">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="serv_status">Publicar agora?</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <div class="radio">
                    <label>
                      <input name="serv_status" id="optionsRadios1" type="radio" value="2" <?php
                      if (isset($post['serv_status']) && $post['serv_status'] == 2): echo 'checked="true"';
                      endif;
                      ?>> Sim
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input name="serv_status" id="optionsRadios2" type="radio" value="1" <?php
                      if (isset($post['serv_status']) && $post['serv_status'] == 1): echo 'checked="true"';
                      endif;
                      ?>> Não
                    </label>
                  </div>                 
                </div>                                 
              </div> 

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <!--inclusão das seleções de categorias-->
                  <?php include("inc/categorias.inc.php"); ?>
                  <!-- /inclusão das seleções de categorias-->
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="serv_cover">Capa <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="file" id="app_cover" name="serv_cover" required="required">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="serv_title">Nome <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="serv_title" name="serv_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                  if (isset($post['serv_title'])): echo $post['serv_title'];
                  endif;
                  ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="serv_description">Descrição, até 160 caractéres <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea name="serv_description" 
                            required="required" 
                            class="form-control col-md-7 col-xs-12 j_contChars" 
                            data-parsley-trigger="keyup" 
                            data-parsley-minlength="140" 
                            data-parsley-maxlength="160" 
                            data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                            data-parsley-validation-threshold="139"><?php
                              if (isset($post['serv_description'])): echo $post['serv_description'];
                              endif;
                              ?></textarea>
                  <div class="j_cont"></div>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="serv_content">Conteúdo da página <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea name="serv_content" id="serv_content" class="j_word form-control col-md-7 col-xs-12" rows="10"><?php
                    if (isset($post['serv_content'])): echo $post['serv_content'];
                    endif;
                    ?></textarea>                                
                </div>
              </div>
            </div>
          </div>

          <div class="x_panel">
            <div class="x_title">
              <h2>Galeria de imagens do item</h2>
              <div class="clearfix"></div>        
            </div>

            <div class="x_content">              
              <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <input id="app_gallery" name="gallery_file[]" type="file" multiple="">
                  <p class="help-block">Selecione até 15 imagens para adicionar ao seu item.</p>
                </div>
              </div> 
            </div>
          </div>

          <div class="x_panel">
            <div class="x_title">
              <div class="col-lg-8 col-xs-12 col-md-12">
                <h2>Anexar arquivos ao item</h2>  
              </div>
              <div class="col-lg-4 col-xs-12 col-md-12">                    
                <div class="btn-group pull-right">                   
                  <button class="btn btn-danger j_sendFileIntataneo"><i class="fa fa-file-pdf-o"></i> Deseja enviar um novo arquivo?</button>
                </div>                  
              </div>   
              <div class="clearfix"></div>   
            </div>

            <div class="x_content"> 

              <div class="col-lg-12 col-xs-12 col-md-12">
                <fieldset>
                  <div class="control-group">
                    <div class="controls col-lg-12 col-xs-12 col-md-12">
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon"><i class="fa fa-search"></i></span>
                        <input type="search" name="buscaFile" data-min-length="3" class="form-control j_itemFileSearch" placeholder="Pesquisar por arquivos..."/>
                      </div>                    
                    </div>
                  </div>             
                </fieldset>  
              </div>               

              <div class="clearfix"></div>
              <?php
              $url_relation = 0;
              include('inc/downloads-inc.php');
              ?>
            </div>

          </div>

        </div>
      </div>
    </div>

    <div class="pull-right">
      <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=servicos/index'"><i class="fa fa-home"></i></button>
    </div>
    <div class="clearfix"></div>
    <br>
  </form>
  <div class="clearfix"></div>
</div>

<!--MODAL PARA ENVIAR DOWNLOADS VIA AJAX E INCLUIR NA LISTA-->
<?php include('inc/modal.download.inc.php'); ?>
<!--/MODAL PARA ENVIAR DOWNLOADS VIA AJAX E INCLUIR NA LISTA-->