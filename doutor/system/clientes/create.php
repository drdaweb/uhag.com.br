<!-- page content -->
<div class="right_col" role="main">  
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <form data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
        <div class="page-title">
            <div class="title col-md-12 col-sm-6 col-xs-12">
                <h3><i class="fa fa-users"></i> Cadastro de clientes</h3>
            </div>
            <div class="clearfix"></div>
            <br/>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="pull-right">
                        <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
                        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=clientes/index'"><i class="fa fa-home"></i></button>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                    if (isset($post) && isset($post['Cadastra'])):
                        unset($post['Cadastra']);
                        $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];
                        $post['cli_cover'] = ( $_FILES['cli_cover']['tmp_name'] ? $_FILES['cli_cover'] : null );

                        $cadastra = new Clientes();
                        $cadastra->ExeCreate($post);

                        if (!$cadastra->getResult()):
                            $erro = $cadastra->getError();
                            WSErro($erro[0], $erro[1], null, $erro[2]);
                        else:
                            $_SESSION['Error'] = $cadastra->getError();
                            header('Location: painel.php?exe=clientes/create&get=true');
                        endif;

                    endif;

                    if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;
                    ?>                    
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="x_title">
                                <h2>Preencha os campos abaixo para cadastrar o cliente</h2>                           
                                <div class="clearfix"></div>                            
                            </div>

                            <div class="clearfix"></div>
                            <br>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cli_status">Publicar agora?</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <div class="radio">
                                        <label>
                                            <input name="cli_status" id="optionsRadios1" type="radio" value="2" <?php
                                            if (isset($post['cli_status']) && $post['cli_status'] == 2): echo 'checked="true"';
                                            endif;
                                            ?>> Sim
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input name="cli_status" id="optionsRadios2" type="radio" value="1" <?php
                                            if (isset($post['cli_status']) && $post['cli_status'] == 1): echo 'checked="true"';
                                            endif;
                                            ?>> Não
                                        </label>
                                    </div>                 
                                </div>                                 
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cat_parent">Sessão ou categoria do item <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <!--inclusão das seleções de categorias-->
                                    <?php include("inc/categorias.inc.php"); ?>
                                    <!-- /inclusão das seleções de categorias-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cli_cover">Capa <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="file" id="app_cover" name="cli_cover" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cli_title">Nome <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="cli_title" name="cli_title" required="required" class="form-control col-md-7 col-xs-12" value="<?php
                                    if (isset($post['cli_title'])): echo $post['cli_title'];
                                    endif;
                                    ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cli_url">Site <span class="text-info">(opcional)</span></label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="cli_url" name="cli_url" class="form-control col-md-7 col-xs-12" value="<?php
                                    if (isset($post['cli_url'])): echo $post['cli_url'];
                                    endif;
                                    ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cli_description">Descrição <span class="required">*</span>                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea name="cli_description" required="required" class="form-control col-md-7 col-xs-12 j_word"><?php
                                              if (isset($post['cli_description'])): echo $post['cli_description'];
                                              endif;
                                              ?></textarea>                                
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <button type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=clientes/index'"><i class="fa fa-home"></i></button>
        </div>
        <div class="clearfix"></div>
        <br>
    </form>
    <div class="clearfix"></div>
</div>
