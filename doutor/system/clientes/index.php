<!-- page content -->
<div class="right_col" role="main">   
    <div class="row">
        <?php
        $lv = 1;
        if (!APP_USERS || empty($userlogin) || $user_level < $lv):
            die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
        endif;
        ?>	
    </div>
    <div class="page-title">
        <div class="title col-md-12 col-sm-6 col-xs-12">
            <h3><i class="fa fa-users"></i> Lista de clientes cadastrados</h3>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">  
                <div class="x_panel">
                    <?php
                    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
                    if (isset($get) && $get == true && isset($_SESSION['Error'])):
                        //COLOCAR ALERTA PERSONALIZADOS
                        WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
                        unset($_SESSION['Error']);
                    endif;
                    ?>

                    <div class="x_content">
                        <div class="x_title">
                            <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode, acessar, editar,remover e alterar status dos clientes</small></h2>                           
                            <div class="clearfix"></div>                            
                        </div>
                        <br/>
                        <div class="table-responsive">
                            <table id="datatable" class="table table-striped dtr-inline dataTable jambo_table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Logo</th>
                                        <th>Nome</th>                                    
                                        <th>Descrição</th>
                                        <th>Status</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $ReadRecursos = new Read;
                                    $ReadRecursos->ExeRead(TB_CLIENTE, "WHERE user_empresa = :id", "id={$_SESSION['userlogin']['user_empresa']}");
                                    if (!$ReadRecursos->getResult()):
                                        WSErro("Nenhum cliente foi encontrado.", WS_INFOR, null, "Aviso!");
                                    else:
                                        foreach ($ReadRecursos->getResult() as $dados):
                                            extract($dados);
                                            ?>
                                            <tr class="j_item" id="<?= $cli_id; ?>">
                                                <th scope="row"><?= $cli_id; ?></th>
                                                <th><a href="<?= BASE . "/uploads/" . $cli_cover; ?>" target="_blank" title="<?= $cli_title; ?>" class="btn border-dark j_modalBox"><?= Check::Image("../doutor/uploads/" . $cli_cover, $cli_title, null, 80); ?></a></th>
                                                <td><?= $cli_title; ?></td>
                                                <td><?= Check::Words($cli_description, 20); ?></td>                                                               
                                                <td class="j_GetStatusRecursos" rel="<?= $cli_id; ?>"></td>                                                               
                                                <td style="width: 110px;">
                                                    <div class="btn-group btn-group-xs">                                      
                                                        <a class="btn btn-dark" href="../clientes#<?= $cli_name; ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                                        <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=clientes/update&id=<?= $cli_id; ?>'"><i class="fa fa-pencil"></i></button>
                                                        <button type="button" class="btn btn-danger j_remove" rel="<?= $cli_id; ?>" action="RemoveCliente"><i class="fa fa-trash"></i></button>
                                                        <button type="button" class="btn j_statusRecursos btn-default" rel="<?= $cli_id; ?>" tabindex="getStatusCli" action="StatusCli" value="<?= $cli_status; ?>"><i class="fa fa-ban"></i></button>
                                                    </div>                                                        
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>