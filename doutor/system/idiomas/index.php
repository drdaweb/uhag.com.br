<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 1;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-language"></i> Lista de idiomas cadastrados na empresa</h3>
    </div>
    <div class="clearfix"></div>

    <?php
    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
    if (isset($get) && $get == true && isset($_SESSION['Error'])):
      //COLOCAR ALERTA PERSONALIZADOS
      WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
      unset($_SESSION['Error']);
    endif;

    $PerPage = filter_input(INPUT_GET, 'perpage', FILTER_VALIDATE_INT);
    $PerPage = (!empty($PerPage) || isset($PerPage) ? $PerPage : 25);

    $Page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);

    $Pager = new Pager("painel.php?exe=idiomas/index&perpage={$PerPage}&page=");
    $Pager->ExePager($Page, $PerPage);
    $ReadRecursos = new Read;

    $post = filter_input(INPUT_POST, 'busca', FILTER_DEFAULT);
    $search = (!empty($post) ? urldecode(strip_tags(trim($post))) : null);
    ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12"> 
        <div class="x_panel">          

          <div class="x_title">

            <h2>Você pode editar, excluir e alterar status dos idiomas.</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
            </ul>

            <div class="clearfix"></div>
            <br class="clearfix" />

            <!--BLOCO COM CAMPO DE PESQUISA E ITENS POR PÁGINA-->
            <?php //include('inc/filter-search.inc.php'); ?>  
            <!--BLOCO COM CAMPO DE PESQUISA E ITENS POR PÁGINA-->

          </div>

          <?php
//          if (!empty($search)):
//            $search = strip_tags(trim(urlencode($search)));
//            header('Location: ' . BASE . '/painel.php?exe=idiomas/index&busca=' . $search);
//          endif;
//          $s = filter_input(INPUT_GET, 'busca', FILTER_DEFAULT);

          $ReadEmps = new Read;

//          if (!empty($s)):
//            $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_status != :st AND empresa_id != :id AND (empresa_parent = :lang OR empresa_id = :cli) AND (empresa_name LIKE '%' :s '%') OR (empresa_razaosocial LIKE '%' :s '%') OR (empresa_site LIKE '%' :s '%')  OR (empresa_id LIKE '%' :s '%') ORDER BY empresa_name ASC LIMIT :limit OFFSET :offset", "st=3&id=" . EMPRESA_MASTER . "&lang={$_SESSION['userlogin']['user_empresa']}&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
//          else:
          $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_idioma_status != :st AND empresa_id != :id AND ( empresa_id = :lang OR empresa_parent = :lang ) ORDER BY empresa_idioma_name ASC", "st=3&id=" . EMPRESA_MASTER . "&lang={$_SESSION['userlogin']['user_empresa']}");
          $reademp = $ReadEmps->getResult();
          $reademp = $reademp[0]['empresa_parent'];
          $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_idioma_status != :st AND empresa_id != :id AND empresa_id = :sess OR ( empresa_idioma_status != :st AND empresa_id != :id AND (empresa_id = :principal OR empresa_parent = :principal)) ORDER BY empresa_idioma_name ASC", "st=3&id=" . EMPRESA_MASTER . "&sess={$_SESSION['userlogin']['user_empresa']}&principal={$reademp}");
//          endif;

          if (!$ReadEmps->getResult()):
//            $Pager->ReturnPage();
            WSErro("Nenhum item foi encontrado em sua pesquisa.", WS_ALERT, null, "Doutores da Web");
          else:
            ?>     
            <div class="x_content">
              <div class="table-responsive">
                <table class="table nowrap">
                  <thead>
                    <tr>
                      <th>Logo</th>
                      <th>Idioma</th>                      
                      <th>Site</th>                      
                      <th>Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($ReadEmps->getResult() as $key): extract($key); ?>
                      <tr class="j_item" id="<?= $empresa_id; ?>">
                        <th><?= Check::Image('../doutor/uploads/' . $empresa_capa, $empresa_name, '', 50); ?></th>
                        <th><h4><?= Check::GetIdioma($empresa_idioma) . ' ' . $empresa_idioma_name; ?></h4></th>                        
                        <td><?= $_SERVER['REQUEST_SCHEME'] . '://' . $empresa_idioma_sg . '.' . Check::GetIdiomaUrl($empresa_idioma_sg) . SYSROOT; ?></td>                   
                        <td>
                          <div class="btn-group-xs btn-group">
                            <button type="button" class="btn btn-dark" onclick="location = '<?= $_SERVER['REQUEST_SCHEME'] . '://' . $empresa_idioma_sg . '.' . Check::GetIdiomaUrl($empresa_idioma_sg) . SYSROOT; ?>'"><i class="fa fa-eye"></i></button>                                                                                                        
                            <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=idiomas/update&id=<?= $empresa_id; ?>'"><i class="fa fa-edit"></i></button>                                                                                                        
                            <button type="button" class="btn btn-danger j_remove" action="RemoveIdioma" rel="<?= $empresa_id; ?>"><i class="fa fa-trash"></i></button> 
                            <!--Botão de status do projeto-->
                            <?= Check::GetSetStatus($empresa_idioma_status, $empresa_id, "StatusIdioma"); ?>
                            <!-- /Botão de status do projeto-->
                          </div>
                        </td>
                      </tr>  
                    <?php endforeach; ?>
                  </tbody>
                </table>

                <div class="clearfix"></div>
                <hr>
                <div class="col-sm-12">
                  <div class="btn-toolbar pull-right">
                    <?php
//                    if (!empty($s)):
//                      $Pager->ExePaginator(TB_EMP, "WHERE empresa_status != :st AND empresa_id != :id AND (empresa_parent = :lang OR empresa_id = :cli) AND (empresa_name LIKE '%' :s '%') OR (empresa_idioma LIKE '%' :s '%') OR (empresa_idioma_name LIKE '%' :s '%')  OR (empresa_id LIKE '%' :s '%') ORDER BY empresa_name ASC LIMIT :limit OFFSET :offset", "st=3&id=" . EMPRESA_MASTER . "&lang={$_SESSION['userlogin']['user_empresa']}&cli={$_SESSION['userlogin']['user_empresa']}&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
//                    else:
//                      $Pager->ExePaginator(TB_EMP, "WHERE empresa_status != :st AND empresa_id != :id AND (empresa_parent = :lang OR empresa_id = :cli)", "st=3&id=" . EMPRESA_MASTER . "&lang={$_SESSION['userlogin']['user_empresa']}&cli={$_SESSION['userlogin']['user_empresa']}");
//                    endif;
//                    echo $Pager->getPaginator();
                    ?>
                  </div>
                </div>
              </div> 
            </div>
          <?php endif; ?>                              
        </div>             
      </div>  
    </div>  
  </div>
  <div class="clearfix"></div>
</div>
<!-- /page content -->

<!--Modal-->
<div class="j_fundoModal none" id="note_control">

  <div class="j_conteudoModal">

    <div class="btn-group pull-right">
      <button type="button" class="btn btn-primary j_clear"><i class="fa fa-file-word-o"></i> Nova</button>
      <button type="button" class="btn btn-danger j_fechar" id="note_control"><i class="fa fa-close"></i> Fechar</button>      
    </div>

    <div class="clearfix"></div>

    <form method="post" enctype="multipart/form-data">
      <div class="x_title">            
        <h2></h2> 
        <div class="clearfix"></div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-12" for="notas_titulo">Título <span class="required">*</span>
        </label>
        <div class="col-md-12">
          <input name="notas_titulo" class="form-control col-md-12" value="<?php
          if (isset($post['notas_titulo'])): echo $post['notas_titulo'];
          endif;
          ?>">                             
        </div>
      </div>

      <div class="clearfix"></div>
      <br/>

      <div class="form-group">
        <label class="control-label col-md-12" for="notas_msg">Conteúdo da nota <span class="required">*</span>
        </label>
        <div class="col-md-12">
          <textarea name="notas_msg" id="prod_content"  class="form-control col-md-7 col-xs-12" rows="5"><?php
          if (isset($post['notas_msg'])): echo $post['notas_msg'];
          endif;
          ?></textarea>                                
        </div>
      </div>

      <div class="clearfix"></div>
      <br/>

    </form>


  </div>    

</div>
<!--/Modal-->