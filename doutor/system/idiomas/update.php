<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 3;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form id="demo-form2" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" data-parsley-validate>
    <div class="page-title">
      <div class="title col-md-12 col-sm-12 col-xs-12">
        <h3><i class="fa fa-language"></i> Cadastro de Idiomas</h3>
      </div>

      <div class="clearfix"></div>
      <div class="pull-right">
        <button type="submit" name="UpdateIdioma" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=idiomas/index'"><i class="fa fa-home"></i></button>
      </div>
      <div class="clearfix"></div> 

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
          <div class="x_panel">          

            <div class="x_title">   
              <h2>Atualize os idiomas cadastrados em seu site.<small>Você pode definir uma bandeira para cada idioma.</small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
              </ul>
              <div class="clearfix"></div>
              <br class="clearfix" />
            </div>

            <?php
            $get = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
            $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if (isset($post) && isset($post['UpdateIdioma'])):
              unset($post['UpdateIdioma']);

              $update = new Idiomas;
              $update->ExeUpdate($get, $post);
              $erro = $update->getError();
              if (!$update->getResult()):
                WSErro($erro[0], $erro[1], null, $erro[2]);
              else:
                WSErro($erro[0], $erro[1], null, $erro[2]);
              endif;
            endif;

            $ReadEmps = new Read;
            $ReadEmps->ExeRead(TB_EMP, "WHERE empresa_id = :id AND (empresa_id = :sess OR empresa_parent = :sess)", "id={$get}&sess={$_SESSION['userlogin']['user_empresa']}");
            if (!$ReadEmps->getResult() && isset($get)):
              WSErro("O idioma não pode ser editado. Tente atualizar a página ou trocar para o idioma principal da empresa (Português).", WS_ALERT, null, "Doutores da Web");
            else:
              $extrair = $ReadEmps->getResult();
              extract($extrair[0]);
              ?>              
              <div class="x_content">  

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma">Bandeira do idioma <span class="required">*</span></label>
                  <div class="col-md-7 col-sm-7 col-xs-12">
                    <div class="input-group">
                      <select id="user_level" name="empresa_idioma" required="required" class="form-control">
                        <option value="" class="j_flags" data-flag="">-- Selecione o país --</option> 
                        <?php
                        $ReadIdioma = new Read;
                        $ReadIdioma->ExeRead(TB_PAISES);
                        if ($ReadIdioma->getResult()):
                          foreach ($ReadIdioma->getResult() as $key => $value):
                            ?>
                            <option value="<?= $value['country_id']; ?>" <?php
                            if (isset($post['empresa_idioma']) && $post['empresa_idioma'] == $value['country_id']):
                              echo 'selected="selected"';
                            elseif ($value['country_id'] == $empresa_idioma):
                              echo 'selected="selected"';
                            endif;
                            ?> class="j_flags" data-flag="<?= $value['country_sigla']; ?>"><?= $value['country_name']; ?></option>
                                    <?php
                                  endforeach;
                                endif;
                                ?>
                      </select>
                      <span class="input-group-addon j_bandeira">
                        <i class="fa fa-flag" aria-hidden="true"></i>
                      </span>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma_name">Nome do idioma <span class="required">*</span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" id="empresa_idioma_name" name="empresa_idioma_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o nome do idioma: Português Brasil" value="<?php
                    if (isset($post['empresa_idioma_name'])): echo $post['empresa_idioma_name'];
                    else: echo $empresa_idioma_name;
                    endif;
                    ?>">
                  </div>
                </div> 


                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma_sg">Sigla do idioma <span class="required">*</span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" id="empresa_idioma_sg" name="empresa_idioma_sg" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o a sigla do idioma: en, br, pt, ar, es" value="<?php
                    if (isset($post['empresa_idioma_sg'])): echo $post['empresa_idioma_sg'];
                    else: echo $empresa_idioma_sg;
                    endif;
                    ?>" data-parsley-trigger="keyup" data-parsley-minlength="2" data-parsley-maxlength="3" data-parsley-maxlength-message="Você deve digitar no máximo 3 caracteres!" data-parsley-validation-threshold="3" maxlength="3">
                  </div>
                </div> 

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empresa_idioma_style">Estilo CSS do idioma</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea id="empresa_idioma_style" name="empresa_idioma_style" rows="10" class="form-control col-md-7 col-xs-12" placeholder="<style> *{ //Seu código aqui } </style>"><?php
                      if (isset($post['empresa_idioma_style'])):
                        echo $post['empresa_idioma_style'];
                      else:
                        echo $empresa_idioma_style;
                      endif;
                      ?></textarea>
                  </div>
                </div>                

              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>    

      <div class="pull-right">
        <button type="submit" name="UpdateIdioma" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=idiomas/index'"><i class="fa fa-home"></i></button>
      </div> 
      <div class="clearfix"></div> 
      <br/>
    </div>
  </form>
</div>

