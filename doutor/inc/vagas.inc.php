<select name="vaga_parent" class="form-control col-md-7 col-xs-12" required="required">
    <?php
    $ReadRecursos = new Read;
    $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_parent IS NULL ORDER BY vaga_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
    ?>
    <option value="" selected="selected" class="text-info text-bold" style="padding: 10px;">-- Selecione --</option>
    <?php
    if ($ReadRecursos->getResult()):
        foreach ($ReadRecursos->getResult() as $key):
            ?>
            <option value="<?= $key['vaga_id'] ?>" <?php
            if (isset($post['vaga_parent']) && $post['vaga_parent'] == $key['vaga_id']): echo 'selected="selected"';
            elseif (isset($dados) && $dados['vaga_parent'] == $key['vaga_id']): echo 'selected="selected"';
            endif;
            ?> style="padding:5px;">» <?= $key['vaga_title']; ?></option>
                    <?php
                    $ReadRecursos->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_parent = :cat ORDER BY vaga_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['vaga_id']}");
                    foreach ($ReadRecursos->getResult() as $key):
                        ?>
                <option value="" style="color: #bbb; padding: 4px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $key['vaga_title']; ?></option>
                <?php
            endforeach;
        endforeach;
    endif;
    ?>
</select>