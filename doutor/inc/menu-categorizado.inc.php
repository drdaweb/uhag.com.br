<?php

$CategMenu[] = array(
  'pasta' => 'idiomas',
  'icone' => 'fa fa-language',
  'titulo' => 'Idiomas <span class="label label-success">Novo</span>',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar'),
  )
);

$CategMenu[] = array(
  'pasta' => 'importador',
  'icone' => 'fa fa-database',
  'titulo' => 'Importar Dados <span class="label label-success">Novo</span>',
  'url' => array(
    array('pagina' => 'index', 'titulo' => 'Importar'),
  )
);

$CategMenu[] = array(
  'pasta' => 'home',
  'icone' => 'fa fa-home',
  'titulo' => 'Página principal <span class="label label-success">Novo</span>',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Criar página'),
    array('pagina' => 'update', 'titulo' => 'Editar'),
//    array('pagina' => 'view', 'titulo' => 'Visualizar')
  )
);

$CategMenu[] = array(
  'pasta' => 'paginas',
  'icone' => 'fa fa-file-text-o',
  'titulo' => 'Páginas <span class="label label-success">Novo</span>',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'newsletter',
  'icone' => 'fa fa-envelope',
  'titulo' => 'Newsletter',
  'url' => array(
//        array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar'),
//        array('pagina' => 'index', 'titulo' => 'Enviar')
  )
);

$CategMenu[] = array(
  'pasta' => 'categorias',
  'icone' => 'fa fa-list-ol',
  'titulo' => 'Categorias',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'quemsomos',
  'icone' => 'fa fa-bank',
  'titulo' => 'Quem somos',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'orcamentos',
  'icone' => 'fa fa-money',
  'titulo' => 'Orçamentos',
  'url' => array(
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'noticias',
  'icone' => 'fa fa-newspaper-o',
  'titulo' => 'Novidades',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'blog',
  'icone' => 'fa fa-quote-left',
  'titulo' => 'Blog',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'produtos',
  'icone' => 'fa fa-shopping-cart',
  'titulo' => 'Produtos',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'servicos',
  'icone' => 'fa fa-cubes',
  'titulo' => 'Serviços',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'banner',
  'icone' => 'fa fa-desktop',
  'titulo' => 'Banner',
  'url' => array(
//        array('pagina' => 'configuracoes', 'titulo' => 'Configurações'),
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar'),
  ),
);

$CategMenu[] = array(
  'pasta' => 'empresas',
  'icone' => 'fa fa-briefcase',
  'titulo' => 'Fornecedores',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'clientes',
  'icone' => 'fa fa-users',
  'titulo' => 'Clientes',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'cases',
  'icone' => 'fa fa-picture-o',
  'titulo' => 'Cases',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'downloads',
  'icone' => 'fa fa-download',
  'titulo' => 'Downloads',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'vagas',
  'icone' => 'fa fa-building-o',
  'titulo' => 'Vagas',
  'url' => array(
    array('pagina' => 'create', 'titulo' => 'Cadastrar'),
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);

$CategMenu[] = array(
  'pasta' => 'candidatos',
  'icone' => 'fa fa-database',
  'titulo' => 'Candidatos',
  'url' => array(
    array('pagina' => 'index', 'titulo' => 'Listar')
  )
);
