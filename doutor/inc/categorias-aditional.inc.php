<div class="col-md-12 col-xs-12" style="max-height: 600px; overflow-x: auto;">  
  <?php
  $ReadRecursos = new Read;
  $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent IS NULL ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
  ?>  
  <div class="list-unstyled text-left">
    <?php
    if ($ReadRecursos->getResult()):
      foreach ($ReadRecursos->getResult() as $key):
        ?>       
        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
          <div class="checkbox">
            <label for="<?= $key['cat_id'] ?>">
              » <input type="checkbox" class="flat" id="<?= $key['cat_id'] ?>" name="cat_parent[]" value="<?= $key['cat_id'] ?>"
              <?php
              $url_re = explode(',', $categorias_arr);
              foreach ($url_re as $urlsr):
                if (isset($post['cat_parent']) && $post['cat_parent'] == $urlsr):
                  echo 'checked="true"';
                elseif ($urlsr == $key['cat_id']):
                  echo 'checked="true"';
                endif;
              endforeach;
              ?>> <?= $key['cat_title']; ?>
            </label>
          </div>
        </div>
        <?php
        $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['cat_id']}");
        if ($ReadRecursos->getResult()):
          foreach ($ReadRecursos->getResult() as $key):
            ?>             
            <div class="col-md-12 col-sm-12 col-xs-12 form-group">
              <div class="checkbox">
                <label for="<?= $key['cat_id'] ?>">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="flat" id="<?= $key['cat_id'] ?>" name="cat_parent[]" value="<?= $key['cat_id'] ?>"
                  <?php
                  $url_re = explode(',', $categorias_arr);
                  foreach ($url_re as $urlsr):
                    if (isset($post['cat_parent']) && $post['cat_parent'] == $urlsr):
                      echo 'checked="true"';
                    elseif ($urlsr == $key['cat_id']):
                      echo 'checked="true"';
                    endif;
                  endforeach;
                  ?>> <?= $key['cat_title']; ?>
                </label>
              </div>
            </div>             
            <?php
            $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent = :cat ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}&cat={$key['cat_id']}");
            if ($ReadRecursos->getResult()):
              foreach ($ReadRecursos->getResult() as $keys):
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                  <div class="checkbox">
                    <label for="<?= $keys['cat_id'] ?>">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&RightArrow; <input type="checkbox" class="flat" id="<?= $keys['cat_id'] ?>" name="cat_parent[]" value="<?= $keys['cat_id'] ?>"
                                                                                          <?php
                                                                                          $url_re = explode(',', $categorias_arr);
                                                                                          foreach ($url_re as $urlsr):
                                                                                            if (isset($post['cat_parent']) && $post['cat_parent'] == $urlsr):
                                                                                              echo 'checked="true"';
                                                                                            elseif ($urlsr == $keys['cat_id']):
                                                                                              echo 'checked="true"';
                                                                                            endif;
                                                                                          endforeach;
                                                                                          ?>> <?= $keys['cat_title']; ?>
                    </label>
                  </div>
                </div>
                <?php
              endforeach;
            endif;

          endforeach;
        endif;

      endforeach;
    endif;
    ?>
  </div>
</div>
