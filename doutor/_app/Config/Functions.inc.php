<?php

//Atributos de inclusão
function Atributos(){
  $att = array(
    1 => "Últimas Novidades",
    2 => "Últimas postagens do Blog",
    3 => "Carrousel de Cliente",
    4 => "Bloco com vídeos",
    5 => "Últimos produtos",
    6 => "Carrousel de Fornecedores",
    7 => "Últimos Cases",
  );
  return $att;
}

//Pegar id do Modulo
function getModuloId($modulos = null) {
  $ids = array(
    'banner' => 1,
    'blog' => 2,
    'candidatos' => 3,
    'cases' => 4,
    'clientes' => 5,
    'downloads' => 6,
    'empresas' => 7,
    'noticias' => 8,
    'orçamentos' => 9,
    'paginas' => 10,
    'produtos' => 11,
    'quemsomos' => 12,
    'servicos' => 13,
    'vagas' => 14,
    'newsletter' => 15,
    'categorias' => 16
  );

  if (!empty($modulos)):
    return $ids[$modulos];
  else:
    return $ids;
  endif;
}

//Status de empresas
function getStatusEmp($Status = null) {
  $StatusEmp = array(
    1 => 'Ativa',
    2 => 'Desativada',
    3 => 'Cancelada',
  );

  if (!empty($Status)):
    return $StatusEmp[$Status];
  else:
    return $StatusEmp;
  endif;
}

//Sim ou não
function getQuestion($Quest = null) {
  $Response = array(
    1 => 'Sim',
    2 => 'Não'
  );

  if (!empty($Quest)):
    return $Response[$Quest];
  else:
    return $Response;
  endif;
}

//Confirmado e não confirmado
function GetStatusNews($Quest = null) {
  $Response = array(
    1 => 'Não',
    2 => 'Sim'
  );

  if (!empty($Quest)):
    return $Response[$Quest];
  else:
    return $Response;
  endif;
}

//Nomenclatura dos niveis de acesso geral
function GetNiveis($NumNivel = null) {
  $Desc = array(
    1 => 'Nível 1 - Colaborador',
    2 => 'Nível 2 - Supervisor',
    3 => 'Nível 3 - Gestor',
    4 => 'Nível 4 - Consultor',
    5 => 'Nível 5 - Diretor',
    6 => 'Nível 6 - Master User',
  );

  if (!empty($NumNivel)):
    return $Desc[$NumNivel];
  else:
    return $Desc;
  endif;
}

//Nomenclatura dos niveis de acesso usuários
function GetNiveisUser($NumNivel = null) {
  $Desc = array(
    1 => 'Nível 1 - Colaborador',
    2 => 'Nível 2 - Supervisor',
    3 => 'Nível 3 - Gestor',
  );

  if (!empty($NumNivel)):
    return $Desc[$NumNivel];
  else:
    return $Desc;
  endif;
}

//Nomenclatura dos niveis de acesso usuários
function GetNiveisFunc($NumNivel = null) {
  $Desc = array(
    4 => 'Nível 4 - Consultor',
    5 => 'Nível 5 - Diretor',
  );

  if (!empty($NumNivel)):
    return $Desc[$NumNivel];
  else:
    return $Desc;
  endif;
}

//Nomenclatura dos status de recursos do sistema para publicações
function GetStatusRecursos($Status = null) {
  $Desc = array(
    1 => '<span class="text-danger">Offline</span>',
    2 => '<span class="text-success">Online</span>',
  );

  if (!empty($Status)):
    return $Desc[$Status];
  else:
    return $Desc;
  endif;
}