<?php

/**
 * Candidatos.class.php [MODEL]
 * Classe responsável por gerir os cadidatos a vagas no banco de dados.
 * @copyright (c) 2016, Rafael da Silva Lima & Doutores da Web
 */
class Candidatos {

    //Tratamento de resultados e mensagens
    private $Result;
    private $Error;
    //Entrada de dados
    private $Data;
    private $File;

    /**
     * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
     * @param array $Data
     */
    public function __construct(array $Data) {
        $this->Data = $Data;

//        $this->CheckUnset();
        $this->CheckData();
        if ($this->Result):
            $this->Create();
        endif;
    }

    /**
     * <b>Retorno de consulta</b>
     * Se não houve consulta ele retorna true boleano ou false para erros
     */
    public function getResult() {
        return $this->Result;
    }

    public function getAnexo() {
        return $this->File;
    }

    public function getArea() {
        return $this->Data;
    }

    /**
     * <b>Mensagens do sistema</b>
     * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
     * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
     */
    public function getError() {
        return $this->Error;
    }

    ########################################
    ########### METODOS PRIVADOS ###########
    ########################################
    //Verifica se algum campo que não é obrigatorio está vazio e remove do array

    private function CheckUnset() {
        if (empty($this->Data['cand_file'])):
            unset($this->Data['cand_file']);
        endif;
        if (empty($this->Data['cand_mensagem'])):
            unset($this->Data['cand_mensagem']);
        endif;
    }

    //Verifica a integridade dos dados e direciona as operações
    private function CheckData() {
        if (in_array('', $this->Data)):
            $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
            $this->Result = false;
        elseif (!Check::Email($this->Data['cand_email'])):
            $this->Error = array("Digite um endereço de e-mail válido.", WS_ALERT, "Alerta!");
            $this->Result = false;
        else:
            $this->File = ( isset($this->Data['cand_file']) ? $this->Data['cand_file'] : null );
            $this->CheckFile();
        endif;
    }

    //Verifica e envia a foto do perfil do usuário para pasta da (int) empresa/user
    private function CheckFile() {
        if (!empty($this->Data['cand_file']['tmp_name'])):

            $Upload = new Upload("doutor/uploads/");
            $FileName = "curriculo-{$this->Data['cand_name']}-" . (substr(md5(time() + $this->Data['cand_title']), 0, 10));
            $Upload->File($this->Data['cand_file'], $FileName, EMPRESA_CLIENTE, 'curriculo', 5);
            if ($Upload->getError()):
                $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
                $this->Result = false;
            else:
                $this->Data['cand_file'] = $Upload->getResult();
                $this->File = 'doutor/uploads/' . $this->Data['cand_file'];
                $this->Result = true;
            endif;
        else:
            $this->Result = true;
        endif;
    }

    //Cadastra os dados do relatorio no banco
    private function Create() {
        $Create = new Create;
        $Create->ExeCreate(TB_CANDIDATO, $this->Data);
        if (!$Create->getResult()):
            $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
            $this->Result = false;
        else:
            $this->GetVagaById();
            $this->Result = true;
            $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
        endif;
    }

    
    private function GetVagaById() {
        $read = new Read;
        $read->ExeRead(TB_VAGA, "WHERE vaga_id = :id AND user_empresa = :emp", "id={$this->Data['cand_vaga']}&emp=" . EMPRESA_CLIENTE);
        if ($read->getResult()):
            $vaga = $read->getResult();
            $this->Data = $vaga[0]['vaga_title'];
        endif;
    }

}
