<?php

/**
 * Downloads.class.php [MODEL]
 * Classe responsável por gerir os downloads no banco de dados.
 * @copyright (c) 2016, Rafael da Silva Lima & Doutores da Web
 */
class Downloads {

  //Tratamento de resultados e mensagens
  private $Result;
  private $Error;
  //Entrada de dados
  private $Data;
  private $LastId;
  private $BaseDir = null;
  private $Id = null;

  /**
   * <b>Responsável por cadastrar os dados no banco, a entrada deve ser um array envelopado
   * @param array $Data
   */
  public function ExeCreate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();
    $this->CheckData();

    if ($this->Result):
      $this->Create();
    endif;
  }

  public function ExeUpdate(array $Data) {
    $this->Data = $Data;
    $this->CheckUnset();

    $this->Id = $this->Data['dow_id'];
    unset($this->Data['dow_id']);

    $this->CheckData();

    if ($this->Result):
      $this->Update();
    endif;
  }

  /**
   * <b>Retorno de consulta</b>
   * Se não houve consulta ele retorna true boleano ou false para erros
   */
  public function getResult() {
    return $this->Result;
  }

  /**
   * <b>Mensagens do sistema</b>
   * Mensagem e tipo de mensagem [0] e [1] pode ser die entre eles.
   * @return array = mensagem do sistema, utilizar o gatilho de erros do sistema para exibir em tela. 
   */
  public function getError() {
    return $this->Error;
  }

  /**
   * <b>Função que retorna ultimo cadastro</b>
   * @return int = Retorna um numero inteiro
   */
  public function getLastId() {
    return $this->LastId;
  }

  ########################################
  ########### METODOS PRIVADOS ###########
  ########################################
  //Verifica se algum campo que não é obrigatorio está vazio e remove do array

  private function CheckUnset() {
    if (isset($this->Data['baseDir'])):
      $this->BaseDir = $this->Data['baseDir'];
      unset($this->Data['baseDir']);
    endif;
    if (empty($this->Data['dow_status'])):
      unset($this->Data['dow_status']);
    endif;
    if (empty($this->Data['dow_file'])):
      unset($this->Data['dow_file']);
    endif;
  }

  //Verifica a integridade dos dados e direciona as operações
  private function CheckData() {
    if (in_array('', $this->Data)):
      $this->Error = array("Existem dados obrigatórios não preenchidos. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif (Check::CategoryRepeat($this->Data['dow_title'], $_SESSION['userlogin']['user_empresa'])):
      $this->Error = array("Já existe uma categoria ou sessão com o titulo: <b>{$this->Data['dow_title']}</b>, tente utilizar outro titulo.", WS_ERROR, "Alerta!");
      $this->Result = false;
    elseif ($this->CheckNome()):
      $this->Error = array("Já existe um download com este mesmo nome, tente outro", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->sendFile();
//      $this->SetNull();
    endif;
  }

  //Verifica os campos que estão vazios e seta eles como null para gravar no banco
  private function SetNull() {
    $this->Data = array_map('trim', $this->Data);
    foreach ($this->Data as $key => $value):
      if (empty($this->Data[$key])):
        $this->Data[$key] = null;
      endif;
    endforeach;
  }

  //Verifica se existem itens repetidos
  private function CheckNome() {
    $this->Data['dow_name'] = Check::Name($this->Data['dow_title']);
    $WHERE = (!empty($this->Id) ? "dow_id != {$this->Id} AND" : null);
    $Read = new Read;
    $Read->ExeRead(TB_DOWNLOAD, "WHERE {$WHERE} dow_name = :cat AND user_empresa = :emp", "cat={$this->Data['dow_name']}&emp={$_SESSION['userlogin']['user_empresa']}");
    if ($Read->getResult()):
      return true;
    else:
      return false;
    endif;
  }

  //Cadastra os dados no banco
  private function Create() {
    $Create = new Create;
    $Create->ExeCreate(TB_DOWNLOAD, $this->Data);
    if (!$Create->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->LastId = $Create->getResult();
      $this->Result = true;
      $this->Error = array("Cadastro realizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Downloads", "Cadastrou o download {$this->Data['dow_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
    endif;
  }

  //Verifica e envia a foto do perfil do usuário para pasta da (int) empresa/user
  private function sendFile() {
    if (!empty($this->Data['dow_file']['tmp_name'])):

      $this->checkFile();
      $Upload = new Upload($this->BaseDir);
      $FileName = "download-{$this->Data['dow_name']}-" . (substr(md5(time() + $this->Data['dow_title']), 0, 10));
//            $Upload->Image($this->Data['dow_file'], $ImgName, 800, $_SESSION['userlogin']['user_empresa'], 'cases');
      $Upload->File($this->Data['dow_file'], $FileName, $_SESSION['userlogin']['user_empresa'], 'downloads', 15);
      if ($Upload->getError()):
        $this->Error = array($Upload->getError(), WS_ERROR, 'Alerta!');
        $this->Result = false;
      else:
        $this->Data['dow_file'] = $Upload->getResult();
        $this->Result = true;
      endif;
    else:
      $this->Result = true;
    endif;
  }

  //Verifica se já existe um arquivo, se sim deleta para enviar outro!
  private function checkFile() {
    if (!empty($this->Id)):
      $readFile = new Read;
      $readFile->ExeRead(TB_DOWNLOAD, "WHERE dow_id = :id AND user_empresa = :emp", "id={$this->Id}&emp={$_SESSION['userlogin']['user_empresa']}");
      $file = $readFile->getResult();
      if ($readFile->getResult()):
        $delFile = $file[0]['dow_file'];
        if (file_exists("uploads/{$delFile}") && !is_dir("uploads/{$delFile}")):
          unlink("uploads/{$delFile}");
        endif;
      endif;
    endif;
  }

  private function Update() {
    $Update = new Update;
    $Update->ExeUpdate(TB_DOWNLOAD, $this->Data, "WHERE user_empresa = :emp AND dow_id = :id", "emp={$_SESSION['userlogin']['user_empresa']}&id={$this->Id}");
    if (!$Update->getResult()):
      $this->Error = array("Ah, não! O sistema se comportou de maneira inesperada. Revise os dados e tente novamente.", WS_ERROR, "Alerta!");
      $this->Result = false;
    else:
      $this->Result = true;
      $this->Error = array("Cadastro atualizado com sucesso.", WS_ACCEPT, "Aviso!");
      Check::SaveHistoric($_SESSION['userlogin']['user_id'], $_SESSION['userlogin']['user_cargo'], $_SESSION['userlogin']['user_level'], "Downloads", "Atualizou o download {$this->Data['dow_title']}", date("Y-m-d H:i:s"));
      $this->Data = null;
      $this->Id = null;
    endif;
  }

}
