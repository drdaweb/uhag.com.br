<?php
ob_start();
session_start();
require('_app/Config.inc.php');
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= RAIZ; ?>/imagens/favicon.png">
        <title><?= SITENAME; ?></title>

        <!-- Bootstrap -->
        <link href="<?= BASE; ?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?= BASE; ?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="<?= BASE; ?>/build/css/custom.min.css" rel="stylesheet">
    </head>

    <body class="login">
        <div class="login_wrapper">
            <section class="login_content">
                <?php
                $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
                if (isset($post) && $post['RecPassword']):
                    unset($post['RecPassword']);
                    $recPass = new RecoveryAccount;
                    
                    $recPass->ExeRecovery($post);
                    $erro = $recPass->getError();
                    if ($recPass->getResult()):
                        WSErro($erro[0], $erro[1], null, $erro[2]);
                    else:
                        WSErro($erro[0], $erro[1], null, $erro[2]);
                    endif;
                endif;
                ?>  
                <div class="separator">
                    <div>
                        <img src="<?= BASE; ?>/images/logo-sig.png" title="<?= SITENAME; ?>" alt="<?= SITENAME; ?>" class="img-responsive"/>
                    </div>
                </div>
                <form method="post">
                    <h1>Recuperar senha</h1>                           
                    <p class="change_link">Digite o endereço de e-mail associado à sua conta para receber as instruções de redefinição de senha.</p>
                    <div class="clearfix"></div>
                    <br />
                    <div>
                        <input type="text" name="user_email" class="form-control" placeholder="E-mail" required/>
                    </div>
                    <div>
                        <input type="submit" name="RecPassword" class="btn btn-default" value="Solicitar">
                        <a href="index.php" class="reset_pass">Lembrou a senha?</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <div>
                            <h1><i class="fa fa-cogs"></i> <?= SITENAME ?></h1>
                            <p>©2016 <?= SITENAME; ?> Todos os direitos reservados.</p>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>       
</body>
</html>
<?php
ob_end_flush();
