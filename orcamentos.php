<?php
if (!$Read):
  $Read = new Read;
endif;

/**
 * <b>Montagem do breadcrumb</b>
 * Pegar urls amigaveis e titulo dessas urls
 */
//Variavel que vai receber as categorias, itens filhos (categoria) e item final.
$arrBreadcrump = array();

if (isset($URL) && !in_array('', $URL)):
  //Armazena sempre o ultimo item da url
  $lastCategory = end($URL);

  foreach ($URL as $paginas => $value):
    if (!empty($value)):

      $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st AND user_empresa = :emp", "nm={$value}&st=2&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):        
        $itemSessao = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
      endif;

    endif;
  endforeach;

endif;

include('inc/head.php');
include('inc/fancy.php');
?>
<script src="<?= BASE; ?>/_cdn/jmask.js"></script>
<script>
  $(function () {
    $('.celular').mask('(99) 9 9999-9999');
    $('.telefone').mask('(99) 9999-9999');
    $('.media').mask('9999,99');
  });
</script>
</head>
<body>

  <?php include('inc/topo.php'); ?>
  <div class="wrapper">
    <main>
      <div class="content">
        <section>
          <!-- Breadcrump -->
          <?php Check::SetBreadcrumb($arrBreadcrump); ?> 
          <h1 class="title"><?php Check::SetTitulo($arrBreadcrump, $URL); ?> </h1>

          <article class="full">
            <?php
            $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if (isset($post) && isset($post['Enviar'])):
              $post['orc_anexo'] = ( $_FILES['orc_anexo']['tmp_name'] ? $_FILES['orc_anexo'] : null );
              unset($post['Enviar']);
              $post['user_empresa'] = EMPRESA_CLIENTE;

              $Contact = new Orcamento($post);
              $error = $Contact->getError();

              if (!$Contact->getResult()):
                WSErro($error[0], $error[1]);
              else:
                include("inc/orcamento-inc.php");
              endif;

            endif;
            ?>            
            <div class="contact">              

              <div class="grid">
                <div class="col-12 col-m-12"><p>Bem vindo, para um orçamento bem detalhados por favor preencha os campos abaixo.</p></div>
                <form enctype="multipart/form-data" method="post" class="orcamento">

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Nome e sobrenome<span>*</span></label>
                    <input type="text" name="orc_nome" placeholder="Seu nome" value="<?php
                    if (isset($post)): echo $post['orc_nome'];
                    endif;
                    ?>" required/>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Empresa:</label>
                    <input type="text" name="orc_empresa" value="<?php
                    if (isset($post['orc_empresa'])): echo $post['orc_empresa'];
                    endif;
                    ?>"/>                  
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">E-mail<span>*</span></label>
                    <input type="email" name="orc_email" placeholder="Seu e-mail" value="<?php
                    if (isset($post)): echo $post['orc_email'];
                    endif;
                    ?>" required/>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Telefone<span>*</span></label>
                    <input type="text" name="orc_telefone" class="telefone" placeholder="(DDD) + Telefone" value="<?php
                    if (isset($post)): echo $post['orc_telefone'];
                    endif;
                    ?>" required/>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Celular<span>*</span></label>
                    <input type="text" name="orc_celular" class="celular" placeholder="(DDD) + Telefone" value="<?php
                    if (isset($post)): echo $post['orc_celular'];
                    endif;
                    ?>" required/>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Como nos conheceu?<span>*</span></label>
                    <select name="orc_question" required>
                      <option value="" selected disabled>-- Selecione --</option>
                      <option value="Google">Google</option>
                      <option value="E-mail Marketing">E-mail Marketing</option>
                      <option value="YouTube">YouTube</option>
                      <option value="Facebook">Facebook</option>
                      <option value="Portal Solar">Portal Solar</option>
                      <option value="Indicação">Indicação</option>
                      <option value="Correio - Mala Direta">Correio - Mala Direta</option>
                      <option value="Revista/Jornal">Revista/Jornal</option>
                      <option value="Banner Internet">Banner Internet</option>
                      <option value="Outro">Outro</option>
                    </select>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Cidade<span>*</span></label>
                    <input type="text" name="orc_cidade" placeholder="Sua Cidade" value="<?php
                    if (isset($post)): echo $post['orc_cidade'];
                    endif;
                    ?>" required/>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Estado<span>*</span></label>
                    <select name="orc_uf" required>
                      <option value="" selected disabled>-- Selecione --</option>
                      <option value="Acre">Acre</option><option value="Alagoas">Alagoas</option><option value="Amapá">Amapá</option><option value="Amazonas">Amazonas</option><option value="Bahia">Bahia</option><option value="Ceará">Ceará</option><option value="Distrito Federal">Distrito Federal</option><option value="Espirito Santo">Espirito Santo</option><option value="Goiás">Goiás</option><option value="Maranhão">Maranhão</option><option value="Mato Grosso do Sul">Mato Grosso do Sul</option><option value="Mato Grosso">Mato Grosso</option><option value="Minas Gerais">Minas Gerais</option><option value="Pará">Pará</option><option value="Paraíba">Paraíba</option><option value="Paraná">Paraná</option><option value="Pernambuco">Pernambuco</option><option value="Piauí">Piauí</option><option value="Rio de Janeiro">Rio de Janeiro</option><option value="Rio Grande do Norte">Rio Grande do Norte</option><option value="Rio Grande do Sul">Rio Grande do Sul</option><option value="Rondônia">Rondônia</option><option value="Roraima">Roraima</option><option value="Santa Catarina">Santa Catarina</option><option value="São Paulo">São Paulo</option><option value="Sergipe">Sergipe</option><option value="Tocantins">Tocantins</option>
                    </select>
                  </div> 

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Média de gasto</label>
                    <input type="text" name="orc_media" data-mask="9999,99" class="media" placeholder="Ex.: R$ 500,00" value="<?php
                    if (isset($post)): echo $post['orc_media'];
                    endif;
                    ?>"/>
                  </div>                  

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Tipo de Segmento:</label>
                    <select name="orc_segmento">
                      <option value="">-- Selecione --</option>
                      <option  value="AÇUCARIA/ALCOOL">AÇUCARIA/ALCOOL</option>
                      <option  value="AGROINDUSTRIA">AGROINDUSTRIA</option>
                      <option  value="ALIMENTICIA">ALIMENTICIA</option>
                      <option  value="ALUMINIO">ALUMINIO</option>
                      <option  value="AUTOMOBILISTICA">AUTOMOBILISTICA</option>
                      <option  value="BLINDAGEM">BLINDAGEM</option>
                      <option  value="BORRACHA">BORRACHA</option>
                      <option  value="CELULOSE PAPEL">CELULOSE PAPEL</option>
                      <option  value="COMERCIAL">COMERCIAL</option>
                      <option  value="ELETRO/ELETRONICA">ELETRO/ELETRONICA</option>
                      <option  value="ENERGIA">ENERGIA</option>
                      <option  value="FARMACEUTICA">FARMACEUTICA</option>
                      <option  value="FUNDICAO">FUNDICAO</option>
                      <option  value="GRAFICA">GRAFICA</option>
                      <option  value="LAVANDERIA">LAVANDERIA</option>
                      <option  value="MACANICA">MACANICA</option>
                      <option  value="METALURGICA">METALURGICA</option>
                      <option  value="MINERADORA">MINERADORA</option>
                      <option  value="OUTROS">OUTROS</option>
                      <option  value="PLÁSTICO">PLÁSTICO</option>
                      <option  value="QUIMICA">QUIMICA</option>
                      <option  value="SIDERURGICA">SIDERURGICA</option>
                      <option  value="TELEFONICA">TELEFONICA</option>
                      <option  value="TEXTIL">TEXTIL</option>
                      <option  value="VIDROS">VIDROS</option>
                      <option  value="ENGENHARIA">ENGENHARIA</option>
                      <option  value="FABRICANTE DE MÁQUINAS">FABRICANTE DE MÁQUINAS</option>
                    </select>           
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Tipo de Aplicação:</label>
                    <select name="orc_aplicacao">
                      <option value="">-- Selecione --</option>
                      <option  value="AR CONDICIONADO">AR CONDICIONADO</option>
                      <option  value="AUTO CLAV">AUTO CLAV</option>
                      <option  value="BANCO DE GELO">BANCO DE GELO</option>
                      <option  value="BANHO DE ZINCO">BANHO DE ZINCO</option>
                      <option  value="BORRACHA">BORRACHA</option>
                      <option  value="CALANDRA">CALANDRA</option>
                      <option  value="CERAMICA">CERAMICA</option>
                      <option  value="CLIMATIZAÇÃO">CLIMATIZAÇÃO</option>
                      <option  value="COMPRESSOR">COMPRESSOR</option>
                      <option  value="EFLUENTES">EFLUENTES</option>
                      <option  value="EXTRUSÃO">EXTRUSÃO</option>
                      <option  value="FERMENTAÇÃO">FERMENTAÇÃO</option>
                      <option  value="FORMADORA DE TUBOS">FORMADORA DE TUBOS</option>
                      <option  value="FORNOS">FORNOS</option>
                      <option  value="GALVANIZAÇÃO">GALVANIZAÇÃO</option>
                      <option  value="INDUÇÃO">INDUÇÃO</option>
                      <option  value="INJETORA">INJETORA</option>
                      <option  value="LAMINADOR">LAMINADOR</option>
                      <option  value="LAVADOR DE GASES">LAVADOR DE GASES</option>
                      <option  value="LAVANDERIA">LAVANDERIA</option>
                      <option  value="MAQUINA SOLDA">MAQUINA SOLDA</option>
                      <option  value="MÁQUINA DE SORVETE">MÁQUINA DE SORVETE</option>
                      <option  value="MÁQUINA TBM">MÁQUINA TBM</option>
                      <option  value="MINERAÇÃO">MINERAÇÃO</option>
                      <option  value="MOINHO">MOINHO</option>
                      <option  value="OUTROS">OUTROS</option>
                      <option  value="PASTEURIZAÇÃO">PASTEURIZAÇÃO</option>
                      <option  value="PRENSAS">PRENSAS</option>
                      <option  value="REATOR">REATOR</option>
                      <option  value="REDUTOR/MOINHO">REDUTOR/MOINHO</option>
                      <option  value="REFRIGERAÇÃO">REFRIGERAÇÃO</option>
                      <option  value="RESF. AREIA">RESF. AREIA</option>
                      <option  value="RESF. MOTORES">RESF. MOTORES</option>
                      <option  value="ROTOMOLDAGEM">ROTOMOLDAGEM</option>
                      <option  value="SOLDA DE PROJEÇÃO">SOLDA DE PROJEÇÃO</option>
                      <option  value="SOLDA PONTO">SOLDA PONTO</option>
                      <option  value="SOLDA POR COSTURA">SOLDA POR COSTURA</option>
                      <option  value="SOPRADORA">SOPRADORA</option>
                      <option  value="TEMPERA">TEMPERA</option>
                      <option  value="TRANSFORMADORES">TRANSFORMADORES</option>
                      <option  value="TRELICAS">TRELICAS</option>
                      <option  value="UNIDADES HIDRÁULICAS">UNIDADES HIDRÁULICAS</option>
                      <option  value="USINAGEM">USINAGEM</option>
                      <option  value="VIDROS">VIDROS</option>
                      <option  value="HIDROPONIA">HIDROPONIA</option>
                      <option  value="RESF. ÓLEO">RESF. ÓLEO</option>
                    </select>       
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Fase de pesquisa</label>
                    <select name="orc_frase">
                      <option value="">-- Selecione --</option>
                      <option value="Já estou pesquisando há algum tempo">Já estou pesquisando há algum tempo</option>
                      <option value="Comecei a pesquisar agora">Comecei a pesquisar agora</option>
                    </select>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Previsão de Instalação</label>
                    <select name="orc_previsao">
                      <option value="">-- Selecione --</option>
                      <option value="Imediatamente">Imediatamente</option>
                      <option value="3 meses">3 meses</option><option value="6 meses">6 meses</option>
                      <option value="1 ano">1 ano</option>
                    </select>
                  </div>

                  <div class="col-6 col-m-12">
                    <label class="col-12 col-m-12">Anexe seu arquivo</label>
                    <input type="file" name="orc_anexo" class="anexo" />
                  </div>


                  <div class="col-12 col-m-12">
                    <label class="col-12 col-m-12">Comentários / Dúvidas <span>*</span></label>
                    <textarea name="orc_mensagem" rows="10" placeholder="Digite sua mensagem" required><?php
                      if (isset($post)): echo $post['orc_mensagem'];
                      endif;
                      ?></textarea>
                  </div>

                  <div class="col-3 col-m-12">                    
                    <input type="submit" value="Solicitar orçamento" name="Enviar" class="btn" />
                  </div>

                </form>
              </div>

              <hr>

              <div class="col-12 col-m-12 orcamento">
                <h3><?php echo $nomeSite . " - " . $slogan; ?></h3>
                <br class="clear"/>
                <div class="col-6 col-m-12">
                  <h4>Endereço da filial</h4>
                  <p><strong><i class="fa fa-map"></i> <?php echo $rua . " - " . $bairro . " - " . $cidade . "-" . $UF . " - " . $cep; ?></strong></p>
                  <p><strong><i class="fa fa-phone-square"></i> <?php echo $ddd . " " . $fone; ?></strong></p>
                  <p><strong><i class="fa fa-envelope-square"></i> <?php echo $emailContato; ?></strong></p>
                </div>
                <div class="col-6 col-m-12">
                  <h4>Endereço da matriz</h4>
                  <p><strong><i class="fa fa-map"></i> <?php echo $rua . " - " . $bairro . " - " . $cidade . "-" . $UF . " - " . $cep; ?></strong></p>
                  <p><strong><i class="fa fa-phone-square"></i> <?php echo $ddd . " " . $fone; ?></strong></p>
                  <p><strong><i class="fa fa-envelope-square"></i> <?php echo $emailContato; ?></strong></p>
                </div>
                <br class="clear"/>
                <br class="clear"/>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7336.352256177301!2d-47.008371!3d-23.163771!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xcb57d4a200be8e91!2sK%C3%B6rper+Equipamentos+Industriais+Ltda.!5e0!3m2!1spt-PT!2sus!4v1459798690372" height="300" allowfullscreen></iframe>
              </div>

            </div>

          </article>
        </section>
      </div>
    </main>
  </div><!-- .wrapper -->
  <?php include('inc/footer.php'); ?>
</body>
</html>