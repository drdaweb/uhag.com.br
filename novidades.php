<?php
if (!$Read):
  $Read = new Read;
endif;

/**
 * <b>Montagem do breadcrumb</b>
 * Pegar urls amigaveis e titulo dessas urls
 */
//Variavel que vai receber as categorias, itens filhos (categoria) e item final.
$arrBreadcrump = array();

if (isset($URL) && !in_array('', $URL)):
  //Armazena sempre o ultimo item da url
  $lastCategory = end($URL);

  foreach ($URL as $paginas => $value):
    if (!empty($value)):
      $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st AND user_empresa = :emp", "nm={$value}&st=2&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):        
        $itemSessao = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
      endif;

      $Read->ExeRead(TB_NOTICIA, "WHERE noti_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $itemName = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemName[0]['noti_title'], 'url' => $itemName[0]['noti_name'], 'parent' => $itemName[0]['cat_parent']);
      endif;
    endif;
  endforeach;

endif;

include('inc/head.php');
include('inc/fancy.php');
?>

</head>
<body>
  <?php include('inc/topo.php'); ?>
  <div class="wrapper">

    <main role="main">
      <div class="content">
        <section itemid="<?= RAIZ; ?>/novidades" itemscope itemtype="http://schema.org/LiveBlogPosting">
          <!-- Breadcrump -->
          <?php Check::SetBreadcrumb($arrBreadcrump); ?> 
          <h1><?php Check::SetTitulo($arrBreadcrump, $URL); ?> </h1>

          <?php include('inc/social-media.php'); ?>
          <div class="clear"></div>

          <article <?php
          if (count($URL) == 1): echo 'class="full"';
          endif;
          ?>>
              <?php
              $categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
              if (!$categ):
                require 'inc/novidades-inc.php';
              else:
                $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_parent = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
                if (!$Read->getResult()):

                  //Dados para categoria pai
                  $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
                  if (!$Read->getResult()):
                    WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
                  else:
                    $category = $Read->getResult();
                    $category = $category[0];
                    $Read->ExeRead(TB_NOTICIA, "WHERE user_empresa = :emp AND noti_status = :stats AND cat_parent = :id ORDER BY noti_date DESC LIMIT 1", "emp=" . EMPRESA_CLIENTE . "&stats=2&id={$category['cat_id']}");
                    $join = $Read->getResult();
                    if (!$Read->getResult()):
                      ?>
                    <br class="clear"/>
                    <div class="htmlchars">
                      <?= $category['cat_description']; ?>
                    </div>                                        
                    <?php
                  endif;
                endif;
                ?>
                <br class="clear"/>
                <hr>
                <ul class="blog">                    
                  <?php
                  $Read->ExeRead(TB_NOTICIA, "WHERE user_empresa = :emp AND cat_parent = :cat AND noti_status = :stats ORDER BY noti_date DESC", "emp=" . EMPRESA_CLIENTE . "&cat={$categ}&stats=2");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $blog):
                      extract($blog);
                      ?>
                      <li class="blog-item blog-item"> 
                        <div class="col-12">
                          <h2><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $noti_name; ?>" title="<?= $noti_title; ?>"><?= $noti_title; ?></a></h2>
                          <p><?= Check::Words($noti_description, 17); ?></p>
                          <p><small><i class="fa fa-calendar"></i> Publicado em: <?= date("d/m/Y", strtotime($noti_date)); ?></small> <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $noti_name; ?>" title="<?= $noti_title; ?>"><i class="fa fa-plus"></i> veja mais</a></p>
                        </div>
                      </li> 
                      <?php
                    endforeach;
                  endif;
                  ?>
                </ul>
              <?php else: ?>                        
                <ul class="blog">                    
                  <?php
                  foreach ($Read->getResult() as $cat):
                    extract($cat);
                    ?>
                    <li class="blog-item">
                      <div class="col-12">
                        <h2><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>"><?= $cat_title; ?></a></h2>                        
                        <p><?= Check::Words($cat_description, 15); ?></p>
                        <p><a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>"><i class="fa fa-plus"></i> veja mais</a></p>
                      </div>
                    </li> 
                    <?php
                  endforeach;
                  $Read->ExeRead(TB_NOTICIA, "WHERE cat_parent = :cat AND user_empresa = :emp", "cat={$categ}&emp=" . EMPRESA_CLIENTE);
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $blog):
                      extract($blog);
                      ?>
                      <li class="blog-item">
                        <div class="col-12">
                          <h2><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $noti_name; ?>" title="<?= $noti_title; ?>"><?= $noti_title; ?></a></h2>
                          <p><?= Check::Words($noti_description, 15); ?></p>
                          <p><small><i class="fa fa-calendar"></i> Publicado em: <?= date("d/m/Y", strtotime($noti_date)); ?></small> <a rel="nofollow" href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $noti_name; ?>" title="<?= $noti_title; ?>"><i class="fa fa-plus"></i> veja mais</a></p>
                        </div>
                      </li> 
                      <?php
                    endforeach;
                  endif;
                  ?>
                </ul>
              <?php
              endif;
            endif;
            ?>
          </article>         
          <?php include('inc/aside.php'); ?>
        </section>
      </div>
    </main>

  </div><!-- .wrapper -->

  <?php include('inc/footer.php'); ?>

</body>
</html>