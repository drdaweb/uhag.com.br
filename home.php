<?
$title      = 'Home';
$desc       = 'home: Em 1927 em Zurick fundou-se a UHAG Metrology (Übersse Handel A G) uma empresa especializada em vendas de máquinas e equipamentos de medição';
$key        = 'Vendas de Máquinas, Equipamentos de Medição, Distribuidor digimess, Distribuidor mitutoyo';
$var        = 'Home';
include('inc/head.php');
?>
<script src="<?=$url?>js/owl.carousel.js"></script>
<script src="<?=$url?>js/slide-carousel.js"></script>
<link rel="stylesheet" href="<?=$url?>css/owl.carousel.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.css">
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <ul class="cd-hero-slider autoplay hide-mobile">
    <li class="selected">
      <div class="cd-full-width">
        <div class="sombra">
          <img src="<?=$url?>imagens/informacoes/distribuidores-equipamentos-medicao-01.jpg" alt="Distribuidores de equipamentos de medição" class="picture-right">
          <h2>Distribuidores de equipamentos de medição</h2>
          <p>Os equipamentos de medição são considerados imprescindíveis para diversos segmentos da indústria, principalmente aqueles voltados à fabricação de equipamentos, máquinas e peça, pois os equipamentos utilizados para medição - além de medir  são responsáveis por examinar determinadas peças e dados e inspecionar e testar se os componentes dos equipamentos estão de acordo com as diretrizes e especificações exigidas em cada projeto.</p>
          <a href="<?=$url?>distribuidores-equipamentos-medicao" class="cd-btn">Saiba mais</a>
        </div>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <div class="sombra">
          <img src="<?=$url?>imagens/informacoes/equipamentos-medicao-01.jpg" alt="Equipamentos para medição" class="picture-right">
          <h2>Equipamentos para medição</h2>
          <p>Muitos profissionais ligados ao meio industrial nem imaginam, no entanto, é mais do que possível que estes espaços contem com a presença de equipamentos para medição importados na busca dos melhores índices de dureza trabalhados em plásticos e outros tipos de estruturas robustas.</p>
          <a href="<?=$url?>equipamentos-medicao" class="cd-btn">Saiba mais</a>
        </div>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <div class="sombra">
          <img src="<?=$url?>imagens/informacoes/durometro-bancada-01.jpg" alt="Durômetro de bancada" class="picture-right">
          <h2>Durômetro de bancada</h2>
          <p>O durômetro de bancada é utilizado principalmente por empresas industriais que necessitam aferir a dureza de suas peças antes de serem completamente produzidas. Desta maneira, o equipamento é considerado um controlador de qualidade, uma vez que detecta as inconformidades das peças a serem fabricadas.</p>
          <a href="<?=$url?>durometro-bancada" class="cd-btn">Saiba mais</a>
        </div>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <div class="content">
    <section class="index-section">
      <div class="wrapper">
        <h1 class="txtcenter"><?=$nomeSite." - ".$slogan?></h1>
        <p>
          Em 1927 em Zurick fundou-se a UHAG Metrology (Übersse Handel A G) uma empresa especializada em vendas de máquinas e equipamentos de medição.
          Em 2015 tornou-se uma empresa dedicada ao fornecimento de Instrumentos e Equipamentos de Medição atuando diretamente junto as empresa de pequeno, médio e grande porte, buscando sempre a liderança na qualidade e tecnologia, onde contamos ainda com peças de reposição e assistência técnica própria com profissionais altamente treinados e em constante reciclagem
        </p>
        <p class="txtcenter"><a href="<?=$url?>empresa" title="Sobre a <?=$nomeSite?>" class="btn"> Saiba mais sobre a <?=$nomeSite?> <i class="fa fa-external-link" aria-hidden="true"></i></a></p>
      </div>
      <div class="conteudo-index">
        <div class="wrapper">
          <h2>Excelência</h2>
          <p>Contamos com uma equipe de telemarketing, na qual fazemos negociações levando nossos produtos para teste junto ao cliente, sendo que nossos produtos são os melhores do mercado mundial.</p>
          <p>Buscamos manter a liderança no segmento com investimentos constantes em qualidade, tecnologia.</p>
        </div>
      </div>
      <div class="wrapper">
        <h2>Informações em Destaque</h2>
        <ul class="thumbnails">
          <li>
            <a rel="nofollow" href="<?=$url;?>calibrador-rosca" title="Calibrador de rosca"><img src="<?=$url;?>imagens/informacoes/calibrador-rosca-01.jpg" alt="Calibrador de rosca" title="Calibrador de rosca"/></a>
            <h2><a href="<?=$url;?>calibrador-rosca" title="Calibrador de rosca">Calibrador de rosca</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>distribuidor-digimess" title="Distribuidor digimess"><img src="<?=$url;?>imagens/informacoes/distribuidor-digimess-01.jpg" alt="Distribuidor digimess" title="Distribuidor digimess"/></a>
            <h2><a href="<?=$url;?>distribuidor-digimess" title="Distribuidor digimess">Distribuidor digimess</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>distribuidores-instrumentos-medicao" title="Distribuidores de instrumentos de medição"><img src="<?=$url;?>imagens/informacoes/distribuidores-instrumentos-medicao-01.jpg" alt="Distribuidores de instrumentos de medição" title="Distribuidores de instrumentos de medição"/></a>
            <h2><a href="<?=$url;?>distribuidores-instrumentos-medicao" title="Distribuidores de instrumentos de medição">Distribuidores de instrumentos de medição</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>calibrador-rosca" title="Calibrador de rosca"><img src="<?=$url;?>imagens/informacoes/calibrador-rosca-01.jpg" alt="Calibrador de rosca" title="Calibrador de rosca"/></a>
            <h2><a href="<?=$url;?>calibrador-rosca" title="Calibrador de rosca">Calibrador de rosca</a></h2>
          </li>
        </ul>
        <br class="clear">
      </div>
    </section>
  </div>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>hero/css/hero.css">
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>