<?php
if (!$Read):
  $Read = new Read;
endif;

/**
 * <b>Montagem do breadcrumb</b>
 * Pegar urls amigaveis e titulo dessas urls
 */
//Variavel que vai receber os itens filhos (categoria)
$arrBreadcrump = array();

if (isset($URL) && !in_array('', $URL)):
  //Armazena sempre o ultimo item da url
  $lastCategory = end($URL);

  foreach ($URL as $paginas => $value):
    if (!empty($value)):
      $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $itemSessao = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
      endif;

      $Read->ExeRead(TB_QUEMSOMOS, "WHERE quem_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $itemName = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemName[0]['quem_title'], 'url' => $itemName[0]['quem_name'], 'parent' => $itemName[0]['cat_parent']);
      endif;
    endif;
  endforeach;

endif;

include('inc/head.php');
include('inc/fancy.php');
?>

</head>
<body>

  <?php include('inc/topo.php'); ?>

  <div class="wrapper">

    <main role="main">
      <div class="content">
        <section>
          <!-- Breadcrump -->
          <?php Check::SetBreadcrumb($arrBreadcrump); ?> 
          <h1><?php Check::SetTitulo($arrBreadcrump, $URL); ?></h1>

          <?php include('inc/social-media.php'); ?>

          <div class="clear"></div> 
          <article <?php
          if (count($URL) == 1): echo 'class="full"';
          endif;
          ?>>
              <?php
              $categ = Check::CatByName($lastCategory, EMPRESA_CLIENTE);
              if (!$categ):
                require 'inc/quem-somos-inc.php';
              else:
                $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_parent = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
                if (!$Read->getResult()):

                  //Dados para categoria pai
                  $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :parent ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$categ}");
                  if (!$Read->getResult()):
                    WSErro("Desculpe, mas não foi encontrando o conteúdo relacionado a esta página, volte mais tarde", WS_INFOR, null, "Aviso!");
                  else:
                    $category = $Read->getResult();
                    $category = $category[0];
                    ?>
                  <div class="picture-full">        
                    <?php if (isset($category['cat_cover'])): ?>
                      <img src="<?= RAIZ . '/doutor/uploads/' . $category['cat_cover']; ?>" title="<?= $category['cat_title']; ?>" alt="<?= $category['cat_title']; ?>"/>                      
                      <?php
                    else:
                      $Read->ExeRead(TB_QUEMSOMOS, "WHERE user_empresa = :emp AND quem_status = :stats AND cat_parent = :id ORDER BY quem_date DESC LIMIT 1", "emp=" . EMPRESA_CLIENTE . "&stats=2&id={$category['cat_id']}");
                      $join = $Read->getResult();
                      if (!$Read->getResult()):
                        ?>
                        <img src="<?= RAIZ . '/doutor/uploads/' . $join[0]['quem_cover']; ?>" title="<?= $category['cat_title']; ?>" alt="<?= $category['cat_title']; ?>"/>                      
                        <?php
                      endif;
                    endif;
                    ?>
                  </div>                                         
                  <div class="htmlchars">
                    <?= $category['cat_content']; ?>
                  </div>                                        
                <?php
                endif;
                ?>
                <br class="clear"/>
                <hr>
                <ul class="thumbnails">                    
                  <?php
                  $Read->ExeRead(TB_QUEMSOMOS, "WHERE user_empresa = :emp AND cat_parent = :cat AND quem_status = :stats ORDER BY quem_date DESC", "emp=" . EMPRESA_CLIENTE . "&cat={$categ}&stats=2");
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $quem):
                      extract($quem);
                      ?>
                      <li> 
                        <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $quem_name; ?>" title="<?= $quem_title; ?>">
                          <?= Check::Image('doutor/uploads/' . $quem_cover, $quem_title, null, 300, 300) ?>
                        </a>
                        <h2><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $quem_name; ?>" title="<?= $quem_title; ?>"><?= $quem_title; ?></a></h2>
                      </li> 
                      <?php
                    endforeach;
                  endif;
                  ?>
                </ul>
              <?php else:
                ?>                        
                <ul class="thumbnails">                    
                  <?php
                  foreach ($Read->getResult() as $cat):
                    extract($cat);
                    ?>
                    <li><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>">
                        <?php
                        //Pega imagem para adicionar à categoria       
                        $Read->ExeRead(TB_QUEMSOMOS, "WHERE user_empresa = :emp AND quem_status = :stats AND cat_parent = :parent ORDER BY quem_date DESC LIMIT 1", "emp=" . EMPRESA_CLIENTE . "&stats=2&parent={$cat_id}");
                        if ($Read->getResult()):
                          foreach ($Read->getResult() as $somos):
                            echo Check::Image('doutor/uploads/' . $somos['quem_cover'], $somos['quem_title'], null, 300, 300);
                          endforeach;
                        else:
                          echo Check::Image('doutor/images/default.png', $somos['quem_title'], null, 300, 300);
                        endif;
                        ?>                                           
                      </a>
                      <h2><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $cat_name; ?>" title="<?= $cat_title; ?>"><?= $cat_title; ?></a></h2>
                    </li> 
                    <?php
                  endforeach;
                  $Read->ExeRead(TB_QUEMSOMOS, "WHERE cat_parent = :cat AND user_empresa = :emp", "cat={$categ}&emp=" . EMPRESA_CLIENTE);
                  if ($Read->getResult()):
                    foreach ($Read->getResult() as $quemsomos):
                      extract($quemsomos);
                      ?>
                      <li>
                        <a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $quem_name; ?>" title="<?= $quem_title; ?>">
                          <?= Check::Image('doutor/uploads/' . $quem_cover, $quem_title, null, 300, 300); ?>                                           
                        </a>
                        <h2><a href="<?= RAIZ . '/' . Check::CatByParent($cat_parent, EMPRESA_CLIENTE) . $quem_name; ?>" title="<?= $quem_title; ?>"><?= $quem_title; ?></a></h2>
                      </li> 
                      <?php
                    endforeach;
                  endif;
                  ?>
                </ul>
              <?php
              endif;
            endif;
            ?>
          </article>  

          <?php include('inc/aside.php'); ?>

        </section>
      </div>
    </main>
  </div><!-- .wrapper -->
  <?php include('inc/footer.php'); ?>
</body>
</html>