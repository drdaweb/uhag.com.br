<?
$h1         = 'Empresa';
$title      = $h1;
$desc       = 'Empresa: Em 1927 em Zurick fundou-se a UHAG Metrology (Übersse Handel A G) uma empresa especializada em vendas de máquinas e equipamentos de medição.';
$key        = 'Vendas de Máquinas, Equipamentos de Medição, Distribuidor digimess, Distribuidor mitutoyo';
include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
    <main>
        <div class="content">
            <section>
                <?=$caminho?>
                <h1><?=$h1?></h1>
                <article class="full">
                    <p>Em 1927 em Zurick fundou-se a UHAG Metrology (Übersse Handel A G) uma empresa especializada em vendas de máquinas e equipamentos de medição.</p>
                    <p>Em 2015 tornou-se uma empresa dedicada ao fornecimento de Instrumentos e Equipamentos de Medição atuando diretamente junto as empresa de pequeno, médio e grande porte, buscando sempre a liderança na qualidade e tecnologia, onde contamos ainda com peças de reposição e assistência técnica própria com profissionais altamente treinados e em constante reciclagem.</p>
                    <p>Contamos com uma equipe de telemarketing, na qual fazemos negociações levando nossos produtos para teste junto ao cliente, sendo que nossos produtos são os melhores do mercado mundial.</p>
                    <p>Buscamos manter a liderança no segmento com investimentos constantes em qualidade, tecnologia.</p>
                    <h2>Missão</h2>
                    <p>“Ser uma empresa diferenciada, mantendo a liderança no nosso segmento”.</p>
                    <h2>Visão</h2>
                    <p>“Satisfazer as necessidades dos clientes, fornecendo produtos e serviços com qualidade”.</p>
                    <h2>Valores</h2>
                    <p>“Nossos valores estão na melhoria contínua, com responsabilidade, confiabilidade e lucratividade”</p>
                    <h2>Política de Qualidade</h2>
                    <p>3 Pilares de nossa Política de Qualidade:</p>
                    <ul class="list">
                        <li>Garantir Resultados;</li>
                        <li>Satisfação do Cliente;</li>
                        <li>Melhoria Contínua</li>
                    </ul>


                    <h2>Estoque</h2>
                    <div class="grid">
                        <div class="col-3">
                            <img src="<?=$url?>imagens/empresa/empresa-estoque-01.jpg" alt="Estoque" title="Estoque">
                        </div>
                        <div class="col-9">
                            <img src="<?=$url?>imagens/empresa/empresa-estoque-02.jpg" class="maxheight380" alt="Estoque" title="Estoque">
                        </div>
                    </div>

                    <p>Contamos com mais de 40.000 itens a pronta entrega</p>

                    <h2>Assistência Técnica</h2>
                    <p>Contamos um departamento especifico de assistência técnica para atender todo o Brasil.</p>
                    <p>Todos os produtos fornecidos pela UHAG, seguem com uma garantia de 12 meses da data da compra</p>




                    <br class="clear">
                </article>
            </section>
        </div>
    </main>
</div>
<? include('inc/footer.php');?>
</body>
</html>