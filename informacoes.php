
    <?php
    include('inc/vetKey.php');
    $h1       = "Informações";
    $title    = "Informações";
    $desc     = "Informações: Em 1927 em Zurick fundou-se a UHAG Metrology (Übersse Handel A G) uma empresa especializada em vendas de máquinas e equipamentos de medição.";
    $var    = "Informação";
    include('inc/head.php');
    ?>
  </head>
  <body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
      <main>
        <div class="content" itemscope itemtype="https://schema.org/Products">
          <section>
            <?=$caminho?>
            <h1><?=$h1?></h1>
            <article class="full">
              <? include('inc/social-media.php');?>
              
                <h2>Conheça todas as Informações da <?=$nomeSite?>:</h2>
                <ul class="thumbnails">
                  <?php
                  foreach ($vetKey as $key => $vetor) {
                    $strInfo = "
                    <li>";
                      $strInfo .= "
                      <a rel=\"nofollow\" href=\"".$url.$vetor['url']."\" title=\"".$vetor['key']."\"><img src=\"".$url."inc/scripts/thumbs.php?w=150&amp;h=150&amp;imagem=".$url."imagens/informacoes/".$vetor['url']."-01.jpg\" alt=\"".$vetor['key']."\" title=\"".$vetor['key']."\"/></a>
                      <h2 itemprop=\"name\"><a href=\"".$url.$vetor['url']."\" title=\"".$vetor['key']."\">".$vetor['key']."</a></h2>";
                      $strInfo .= "
                    </li>";
                    echo $strInfo;
                  }
                  ?>
                </ul>
              
            </article>
          </section>
        </div>
      </main>
    </div><!-- .wrapper -->
    <? include('inc/footer.php');?>
  </body>
  </html>
  