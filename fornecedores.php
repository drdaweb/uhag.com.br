<?php
if (!$Read):
  $Read = new Read;
endif;

/**
 * <b>Montagem do breadcrumb</b>
 * Pegar urls amigaveis e titulo dessas urls
 */
//Variavel que vai receber os itens filhos (categoria)
$arrBreadcrump = array();

if (isset($URL) && !in_array('', $URL)):
  //Armazena sempre o ultimo item da url
  $lastCategory = end($URL);

  foreach ($URL as $paginas => $value):
    if (!empty($value)):
      $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st AND user_empresa = :emp", "nm={$value}&st=2&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):        
        $itemSessao = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
      endif;

      $Read->ExeRead(TB_EMPRESA, "WHERE emp_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $itemName = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemName[0]['emp_title'], 'url' => $itemName[0]['emp_name'], 'parent' => $itemName[0]['cat_parent']);
      endif;
    endif;
  endforeach;

endif;

include('inc/head.php');
include('inc/fancy.php');
?>


</head>
<body>

  <?php include('inc/topo.php'); ?>

  <div class="wrapper">

    <main role="main">
      <div class="content">
        <section>
          <!-- Breadcrump -->
          <?php Check::SetBreadcrumb($arrBreadcrump); ?> 
          <h1><?php Check::SetTitulo($arrBreadcrump, $URL); ?></h1>

          <?php include('inc/social-media.php'); ?>

          <div class="clear"></div> 

          <article class="full">  
            <?php
            $Read->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_status = :stats AND cat_id = :id ORDER BY cat_date DESC", "emp=" . EMPRESA_CLIENTE . "&stats=2&id=" . Check::CatByName($lastCategory, EMPRESA_CLIENTE));
            if ($Read->getResult()):
              foreach ($Read->getResult() as $CapaCateg):
                ?>
                <div class="picture-full">   
                  <img src="<?= RAIZ . '/doutor/uploads/' . $CapaCateg['cat_cover']; ?>" title="<?= $CapaCateg['cat_title']; ?>" alt="<?= $CapaCateg['cat_title']; ?>"/>                      
                </div>  
                <br class="clear"/>
                <hr>
                <div class="htmlchars">
                  <?= $CapaCateg['cat_content']; ?>
                </div> 
                <?php
              endforeach;
            endif;
            ?>

            <ul class="thumb-clientes">                    
              <?php
              $Read->ExeRead(TB_EMPRESA, "WHERE user_empresa = :emp AND emp_status = :stats ORDER BY emp_title ASC", "emp=" . EMPRESA_CLIENTE . "&stats=2");
              if ($Read->getResult()):
                foreach ($Read->getResult() as $prod):
                  extract($prod);
                  ?>
                  <li>                        
                    <a href="<?= RAIZ . '/doutor/uploads/' . $emp_cover; ?>" title="<?= $emp_title; ?>" class="lightbox">
                      <?= Check::Image('doutor/uploads/' . $emp_cover, $emp_description, null, 400, 100); ?>
                    </a>
                    <h2><?= $emp_title; ?></h2>  
                    <h3><?= $emp_url; ?></h3>
                  </li> 
                  <?php
                endforeach;
              endif;
              ?>
            </ul>
            <br class="clear" />   
            <?php include('inc/social-media.php'); ?>
            <br class="clear" />   
          </article>                   
        </section>
      </div>
    </main>

  </div><!-- .wrapper -->

  <?php include('inc/footer.php'); ?>

</body>
</html>