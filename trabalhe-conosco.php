<?php
if (!$Read):
  $Read = new Read;
endif;

/**
 * <b>Montagem do breadcrumb</b>
 * Pegar urls amigaveis e titulo dessas urls
 */
//Variavel que vai receber os itens filhos (categoria)
$arrBreadcrump = array();

if (isset($URL) && !in_array('', $URL)):
  //Armazena sempre o ultimo item da url
  $lastCategory = end($URL);

  foreach ($URL as $paginas => $value):
    if (!empty($value)):
      $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :st AND user_empresa = :emp", "nm={$value}&st=2&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):        
        $itemSessao = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemSessao[0]['cat_title'], 'url' => $itemSessao[0]['cat_name'], 'parent' => $itemSessao[0]['cat_id']);
      endif;

      $Read->ExeRead(TB_VAGA, "WHERE vaga_name = :nm AND user_empresa = :emp", "nm={$value}&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $itemName = $Read->getResult();
        $arrBreadcrump[] = array('titulo' => $itemName[0]['vaga_title'], 'url' => $itemName[0]['vaga_name'], 'parent' => $itemName[0]['cat_parent']);
      endif;
    endif;
  endforeach;

endif;

include('inc/head.php');
?>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?= BASE; ?>/_cdn/modalbox/app_fancy/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Add fancyBox -->
<link rel="stylesheet" href="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?= BASE; ?>/_cdn/modalbox/app_fancy/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="<?= BASE; ?>/_cdn/jmask.js"></script>
<script>
  $(function () {
    $('.telefone').mask('(99) 9 9999-9999');
  });
</script>
</head>
<body>
  <?php include('inc/topo.php'); ?>
  <div class="wrapper">

    <main role="main">
      <div class="content">
        <section>
          <!-- Breadcrump -->
          <?php Check::SetBreadcrumb($arrBreadcrump); ?> 
          <h1><?php Check::SetTitulo($arrBreadcrump, $URL); ?> </h1>

          <?php include('inc/social-media.php'); ?>
          <div class="clear"></div>

          <article class="full">
            <?php
            $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
            if (isset($post) && isset($post['Enviar'])):
              $post['cand_file'] = ( $_FILES['cand_file']['tmp_name'] ? $_FILES['cand_file'] : null );
              unset($post['Enviar']);
              $post['user_empresa'] = EMPRESA_CLIENTE;

              $Contact = new Candidatos($post);
              $error = $Contact->getError();

              if (!$Contact->getResult()):
                WSErro($error[0], $error[1]);
              else:
                $post['cand_file'] = $Contact->getAnexo();
                $post['cand_vaga'] = $Contact->getArea();
                $post['cand_estagio'] = getQuestion($post['cand_estagio']);
                //Script de envio
                include("inc/candidatos-inc.php");
              endif;
            endif;
            ?>            
            <div class="grid">
              <div class="col-12 col-m-12">
                <div class="htmlchars">
                  <?= $itemSessao[0]['cat_content']; ?>
                </div>
                <br class="clear"/>
                <hr>                    
                <h2>Vagas</h2>
                <?php include ('inc/vagas-inc.php') ?>

                <div class="col-12 col-m-12">
                  <h2>Candidate-se</h2>                
                  <form enctype="multipart/form-data" method="post" class="orcamento">

                    <div class="col-12 col-m-12">
                      <label class="col-12 col-m-12">Estágio? <span>*</span></label>
                      <input type="radio" name="cand_estagio" value="1" required <?php
                      if (isset($post['cand_estagio']) && $post['cand_estagio'] == 1): echo 'checked=""';
                      endif;
                      ?>> <span>Sim</span>
                      <input type="radio" name="cand_estagio" value="2" <?php
                      if (isset($post['cand_estagio']) && $post['cand_estagio'] == 2): echo 'checked=""';
                      endif;
                      ?>> <span>Não</span>
                    </div>

                    <div class="col-6 col-m-12">
                      <label class="col-12 col-m-12">Nome <span>*</span></label>
                      <input type="text" name="cand_nome" value="<?php
                      if (isset($post['cand_nome'])): echo $post['cand_nome'];
                      endif;
                      ?>" required/>
                    </div>

                    <div class="col-6 col-m-12">
                      <label class="col-12 col-m-12">E-mail <span>*</span></label>
                      <input type="text" name="cand_email" value="<?php
                      if (isset($post['cand_email'])): echo $post['cand_email'];
                      endif;
                      ?>" required/>
                    </div>

                    <div class="col-6 col-m-12">
                      <label class="col-12 col-m-12">DDD/Telefone <span>*</span></label>                                        
                      <input type="text" name="cand_telefone" class="telefone" data-mask="(99) 9 9999-9999" value="<?php
                      if (isset($post['cand_telefone'])): echo $post['cand_telefone'];
                      endif;
                      ?>" required/>
                    </div>

                    <div class="col-6 col-m-12">
                      <label class="col-12 col-m-12">Área? <span>*</span></label>
                      <select name="cand_vaga" required>
                        <option value="">-- Selecione --</option>
                        <?php
                        $Read->ExeRead(TB_VAGA, "WHERE user_empresa = :emp AND vaga_status = :stats AND vaga_parent IS NULL ORDER BY vaga_title ASC", "emp=" . EMPRESA_CLIENTE . "&stats=2");
                        if (!$Read->getResult()):
                          ?>
                          <option value="Nenhuma área disponível">Não há áreas no momento</option>
                          <?php
                        else:
                          foreach ($Read->getResult() as $dados):
                            extract($dados);
                            ?>
                            <option value="<?= $vaga_id; ?>" <?php
                            if (isset($post['cand_vaga']) && $post['cand_vaga'] == $vaga_id): echo 'selected="selected"';
                            endif;
                            ?>><?= $vaga_title; ?></option>
                                    <?php
                                  endforeach;
                                endif;
                                ?>
                      </select>
                    </div>

                    <div class="col-6 col-m-12">
                      <label class="col-12 col-m-12">Cargo <span>*</span></label>
                      <input type="text" name="cand_cargo" value="<?php
                      if (isset($post['cand_cargo'])): echo $post['cand_cargo'];
                      endif;
                      ?>" required/>
                    </div>

                    <div class="col-12 col-m-12">
                      <label class="col-12 col-m-12">Anexe seu currículo (Apenas arquivos em PDF) <span>*</span></label>
                      <input type="file" name="cand_file" required>
                    </div>

                    <div class="col-3 col-m-12">                    
                      <input type="submit" value="Enviar candidatura" name="Enviar" class="btn" />
                    </div>

                  </form>
                </div>

              </div>
            </div>            

          </article>

        </section>
      </div>
    </main>
  </div>
  <!-- Footer -->
  <?php include 'inc/footer.php'; ?>
</body>
</html>