
  <?php
  $vetKey = array();
  
    $vetKey[0] = array("url" => "calibrador-rosca", "key" => "Calibrador de rosca", "desc" =>"");
    
    $vetKey[1] = array("url" => "calibrador-rosca-anel", "key" => "Calibrador de rosca anel", "desc" =>"");
    
    $vetKey[2] = array("url" => "calibrador-rosca-digimess", "key" => "Calibrador de rosca digimess", "desc" =>"");
    
    // $vetKey[3] = array("url" => "calibrador-rosca-mitutoyo", "key" => "Calibrador de rosca mitutoyo", "desc" =>"");
    
    $vetKey[4] = array("url" => "calibrador-rosca-preco", "key" => "Calibrador de rosca preço", "desc" =>"");
    
    $vetKey[5] = array("url" => "calibrador-rosca-tampao", "key" => "Calibrador de rosca tampão", "desc" =>"");
    
    $vetKey[6] = array("url" => "distribuidor-digimess", "key" => "Distribuidor digimess", "desc" =>"");
    
    // $vetKey[7] = array("url" => "distribuidor-mitutoyo", "key" => "Distribuidor mitutoyo", "desc" =>"");
    
    $vetKey[8] = array("url" => "distribuidores-equipamentos-medicao", "key" => "Distribuidores de equipamentos de medição", "desc" =>"");
    
    $vetKey[9] = array("url" => "distribuidores-instrumentos-medicao", "key" => "Distribuidores de instrumentos de medição", "desc" =>"");
    
    $vetKey[10] = array("url" => "durometro-bancada", "key" => "Durômetro de bancada", "desc" =>"");
    
    $vetKey[11] = array("url" => "durometro-bancada-digimess", "key" => "Durômetro de bancada digimess", "desc" =>"");
    
    // $vetKey[12] = array("url" => "durometro-bancada-mitutoyo", "key" => "Durômetro de bancada mitutoyo", "desc" =>"");
    
    $vetKey[13] = array("url" => "durometro-bancada-preco", "key" => "Durômetro de bancada preço", "desc" =>"");
    
    $vetKey[14] = array("url" => "durometro-digital", "key" => "Durômetro digital", "desc" =>"");
    
    $vetKey[15] = array("url" => "durometro-digital-bancada", "key" => "Durômetro digital de bancada", "desc" =>"");
    
    $vetKey[16] = array("url" => "durometro-digital-digimess", "key" => "Durômetro digital digimess", "desc" =>"");
    
    // $vetKey[17] = array("url" => "durometro-digital-mitutoyo", "key" => "Durômetro digital mitutoyo", "desc" =>"");
    
    $vetKey[18] = array("url" => "durometro-plasticos", "key" => "Durômetro para plásticos", "desc" =>"");
    
    $vetKey[19] = array("url" => "durometro-portatil-metais", "key" => "Durômetro portátil para metais", "desc" =>"");
    
    // $vetKey[20] = array("url" => "equipamentos-medicao-mitutoyo", "key" => "Equipamentos de medição mitutoyo", "desc" =>"");
    
    $vetKey[21] = array("url" => "equipamentos-medicao", "key" => "Equipamentos para medição", "desc" =>"");
    
    // $vetKey[22] = array("url" => "fornecedor-mitutoyo", "key" => "Fornecedor mitutoyo", "desc" =>"");
    
    $vetKey[23] = array("url" => "fornecedores-instrumentos-medicao", "key" => "Fornecedores de instrumentos de medição", "desc" =>"");
    
    $vetKey[24] = array("url" => "instrumentos-medicao-digimess", "key" => "Instrumentos de medição digimess", "desc" =>"");
    
    // $vetKey[25] = array("url" => "instrumentos-medicao-mitutoyo", "key" => "Instrumentos de medição mitutoyo", "desc" =>"");
    
    $vetKey[26] = array("url" => "instrumentos-medicao", "key" => "Instrumentos para medição", "desc" =>"");
    
    $vetKey[27] = array("url" => "micrometro-analogico", "key" => "Micrômetro analógico", "desc" =>"");
    
    $vetKey[28] = array("url" => "micrometro-digimess", "key" => "Micrômetro digimess", "desc" =>"");
    
    $vetKey[29] = array("url" => "micrometro-digital", "key" => "Micrômetro digital", "desc" =>"");
    
    $vetKey[30] = array("url" => "micrometro-medicao", "key" => "Micrômetro medição", "desc" =>"");
    
    // $vetKey[31] = array("url" => "micrometro-mitutoyo", "key" => "Micrômetro mitutoyo", "desc" =>"");
    
    $vetKey[32] = array("url" => "microscopio-optico-binocular", "key" => "Microscópio óptico binocular", "desc" =>"");
    
    $vetKey[33] = array("url" => "microscopio-optico-binocular-preco", "key" => "Microscópio óptico binocular preço", "desc" =>"");
    
    $vetKey[34] = array("url" => "microscopio-optico-comprar", "key" => "Microscópio óptico comprar", "desc" =>"");
    
    $vetKey[35] = array("url" => "microscopio-optico-preco", "key" => "Microscópio óptico preço", "desc" =>"");
    
    $vetKey[36] = array("url" => "microscopio-optico-trinocular", "key" => "Microscópio óptico trinocular", "desc" =>"");
    
    $vetKey[37] = array("url" => "microscopio-optico-trinocular-preco", "key" => "Microscópio óptico trinocular preço", "desc" =>"");
    
    $vetKey[38] = array("url" => "paquimetro-analogico", "key" => "Paquímetro analógico", "desc" =>"");
    
    // $vetKey[39] = array("url" => "paquimetro-analogico-mitutoyo", "key" => "Paquímetro analógico mitutoyo", "desc" =>"");
    
    $vetKey[40] = array("url" => "paquimetro-analogico-preco", "key" => "Paquímetro analógico preço", "desc" =>"");
    
    $vetKey[41] = array("url" => "paquimetro-digital", "key" => "Paquímetro digital", "desc" =>"");
    
    $vetKey[42] = array("url" => "paquimetro-digital-digimess", "key" => "Paquímetro digital digimess", "desc" =>"");
    
    // $vetKey[43] = array("url" => "paquimetro-digital-mitutoyo", "key" => "Paquímetro digital mitutoyo", "desc" =>"");
    
    $vetKey[44] = array("url" => "paquimetro-digital-preco", "key" => "Paquímetro digital preço", "desc" =>"");
    
    // $vetKey[45] = array("url" => "paquimetro-mitutoyo", "key" => "Paquímetro mitutoyo", "desc" =>"");
    
    $vetKey[46] = array("url" => "rugosimetro-digimess", "key" => "Rugosímetro digimess", "desc" =>"");
    
    $vetKey[47] = array("url" => "rugosimetro-digital", "key" => "Rugosímetro digital", "desc" =>"");
    
    $vetKey[48] = array("url" => "rugosimetro-portatil", "key" => "Rugosímetro portátil", "desc" =>"");
    
    $vetKey[49] = array("url" => "rugosimetro-preco", "key" => "Rugosímetro preço", "desc" =>"");
    
  ?>
  