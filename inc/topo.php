<header id="header-hero">
    <div class="wrapper">
        <? include('inc/barra.inc.php');?>
        <div class="logo"><a rel="nofollow" href="<?=$url?>" title="Voltar a página inicial"><img src="<?=$url?>imagens/logo.png" alt="<?=$slogan." - ".$nomeSite?>" title="<?=$slogan." - ".$nomeSite?>"></a></div>
        <div class="right">
            <a rel="nofollow" class="tel" title="Clique e ligue" href="tel:<?=$ddd.$fone?>"><?=$ddd?> <strong><?=$fone?></strong></a><br>
            
            <?php
            if(isset($fone2) && !empty($fone2)) { echo "<br class=\"hide-tablet hide-mobile\"> <a rel=\"nofollow\" class=\"tel\" title=\"Clique e ligue\" href=\"tel:$ddd$fone2\">$ddd <strong>$fone2</strong></a>";}
            if(isset($fone3) && !empty($fone3)) { echo "<br class=\"hide-tablet hide-mobile\"> <a rel=\"nofollow\" class=\"tel\" title=\"Clique e ligue\" href=\"tel:$ddd$fone3\">$ddd <strong>$fone3</strong></a>";}
            ?><br><br>
            <a rel="nofollow" class="social-icons" href="mailto:<?=$emailContato?>" target="_blank" title="Envie um E-mail"><i class="fa fa-envelope fa-x3"></i> vendas@uhag.com.br</a>
        </div>
    </div>
    <nav id="menu">
        <div class="wrapper">
            <ul >
                <? include('inc/menu-top.php');?>
            </ul>
        </div>
    </nav>
    <div class="clear"></div>
</header>