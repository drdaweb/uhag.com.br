<?
$nomeSite			= 'UHAG Metrology';
$slogan				= 'Vendas de Máquinas e Equipamentos de Medição';

//Pasta do projeto, vazio caso publicado
define('WEBROOT', ''); 
$url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/' . WEBROOT;

$ddd				= '19';
$fone				= '4062-7474';
$fone2				= '4141-7754';
// $fone3				= '2222-5555';
$emailContato		= 'vendas@uhag.com.br';
$rua				= 'R. Salvador Rotela, 61';
$bairro				= 'Santa Rosa';
$cidade				= 'Vinhedo';
$UF					= 'SP';
$cep				= 'CEP: 13280-000';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idCliente			= '101040';
$idAnalytics		= 'UA-118574624-43';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br

//MONTAGEM DA URL PÁGINA PARA O HTACESS NOVO
$getURL = trim(strip_tags(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$urlPagina = explode("/", $getURL);
$ContPag = sizeof($urlPagina);
$urlPagina = $urlPagina[0];
$urlPagina == "index" ? $urlPagina = "" : "";

$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6LdYilkUAAAAAA08j7Sp2OQA5WZBb2hMzKgZk6RT';
$secretKey = '6LdYilkUAAAAAD7RaTkHla2EpKpYSH6VT9VqYqB5';

//Breadcrumbs
$caminho 			= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
</div>
';

$caminho2	= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';

//Pasta de imagens, Galeria, url Facebook, etc.
$pasta 				= 'imagens/informacoes/';
//Redes sociais
$idFacebook			= 'Colocar o ID da página do Facebook'; //Link para achar o ID da página do Facebook http://graph.facebook.com/Nome da página do Facebook
$idGooglePlus		= 'http://plus.google.com.br'; // ID da página da empresa no Google Plus
$paginaFacebook		= 'uhagmetrology';
$author = ''; // Link do perfil da empresa no g+ ou deixar em branco
//Reescrita dos links dos telefones
$link_tel = str_replace('(', '', $ddd);
$link_tel = str_replace(')', '', $link_tel);
$link_tel = str_replace('11', '', $link_tel);
$link_tel .= '5511'.$fone;
$link_tel = str_replace('-', '', $link_tel);
$creditos			= 'Doutores da Web - Marketing Digital';
$siteCreditos		= 'www.doutoresdaweb.com.br';
$caminhoBread 			= '
<div class="title-breadcrumb">
	<div class="wrapper">
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
		<h1>'.$h1.'</h1>
	</div>
</div>
';
$caminhoBread2	= '
<div class="title-breadcrumb">
	<div class="wrapper">
		<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
			<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
			<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
				<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
				</div>
			</div>
		</div>
		<h1>'.$h1.'</h1>
	</div>
</div>
';
?>