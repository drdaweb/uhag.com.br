<?php
//Montagem da busca
$search = filter_input(INPUT_POST, 'busca', FILTER_DEFAULT);
if (!empty($search)):
  $search = strip_tags(trim(urlencode($search)));
  header('Location: ' . RAIZ . '/pesquisa/' . $search);
endif;

//Inclusão da geral com dados e variaveis do cliente
include('inc/geral.php');

//Verificação se a categoria ou página existe no banco de dados, se existir será carregado o head-seo com as infos vindo do Objeto SEO.
$pagina = false;

if (isset($URL) && !in_array('', $URL)):
  //Armazena sempre o ultimo item da url
  $lastCategory = end($URL);

  foreach ($URL as $paginas => $value):
    if (!empty($value)):

      $Read->ExeRead(TB_PAGINA, "WHERE pag_name = :nm AND pag_status = :status AND user_empresa = :emp", "nm={$value}&status=2&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $pagina = true;
      endif;

      $Read->ExeRead(TB_CATEGORIA, "WHERE cat_name = :nm AND cat_status = :status AND user_empresa = :emp", "nm={$value}&status=2&emp=" . EMPRESA_CLIENTE);
      if ($Read->getResult()):
        $pagina = true;
      endif;

    endif;
  endforeach;
endif;

if (!$pagina):
  ?>
  <!DOCTYPE html>
  <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
  <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
  <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
  <!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
      <meta charset="utf-8">

      <script src="<?= RAIZ; ?>/js/jquery.js"></script>
      <script async src="<?= RAIZ; ?>/js/vendor/modernizr-2.6.2.min.js"></script>
      <link rel="stylesheet" href="<?= RAIZ; ?>/css/normalize.css">
      <link rel="stylesheet" href="<?= RAIZ; ?>/css/style.css">
      <link rel="stylesheet" href="<?= RAIZ; ?>/css/doutor.css">
      <!--<link rel="stylesheet" href="<?= RAIZ; ?>/css/simple-grid.css">-->
      <link rel="stylesheet" href="<?= RAIZ; ?>/css/font-awesome.css">
      <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700'>

      <!-- BOTAO SCROLL -->
      <script async src="<?= RAIZ; ?>/js/jquery.scrollUp.min.js"></script>
      <script async src="<?= RAIZ; ?>/js/scroll.js"></script>
      <!-- /BOTAO SCROLL -->

      <!-- MENU  MOBILE -->
      <script async src="<?= RAIZ; ?>/js/jquery.slicknav.js"></script>
      <!-- /MENU  MOBILE -->

      <title><?= $title . " - " . $nomeSite ?></title>
      <base href="<?= RAIZ; ?>">
      <meta name="description" content="<?= $desc ?>">
      <meta name="keywords" content="<?= $h1 . ", " . $key ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximun-scale=1.0">

      <meta name="geo.position" content="<?= $latitude . ";" . $longitude ?>"> 
      <meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>"> 
      <meta name="geo.region" content="<?= $UF ?>-BR">
      <meta name="ICBM" content="<?= $latitude . ";" . $longitude ?>">
      <meta name="robots" content="index,follow">
      <meta name="rating" content="General">
      <meta name="revisit-after" content="7 days">
      <link rel="canonical" href="<?= RAIZ . '/' . $urlPagina; ?>">
      <?php
      if (empty($author)):
        echo '<meta name="author" content="' . $nomeSite . '">';
      else:
        echo '<link rel="author" href="' . $author . '">';
      endif;
      ?>
      <link rel="shortcut icon" href="<?= RAIZ; ?>/imagens/favicon.png">

      <meta property="og:region" content="Brasil">
      <meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
      <meta property="og:type" content="article">
      <?php if (file_exists($url . $pasta . $urlPagina . "-01.jpg")): ?>
      	<meta property="og:image" content="<?= RAIZ . '/' . $pasta . $urlPagina ?>-01.jpg">
  	  <?php endif; ?>
      <meta property="og:url" content="<?= RAIZ . '/' . $urlPagina ?>">
      <meta property="og:description" content="<?= $desc ?>">
      <meta property="og:site_name" content="<?= $nomeSite ?>">
      <meta property="fb:admins" content="<?= $idFacebook ?>">
      <?php
    else:
      require 'inc/head-seo.php';
    endif;
    ?>
    <!-- Sweet Alert-->
    <link rel="stylesheet" href="<?= BASE; ?>/vendors/bootstrap-sweetalert/lib/sweet-alert.css">
    <script src="<?= BASE; ?>/vendors/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
    <script src="<?= RAIZ; ?>/js/ajax.js"></script>
        
    <!--FLAG ICONS-->
    <link href="<?= RAIZ; ?>/doutor/vendors/flag-icon-css-master/css/flag-icon.css" rel="stylesheet">
    <!--/FLAG ICONS-->
    
    <!--CLASSE ASSOCIADA AO IDIOMA-->
    <?= Check::SetIdiomaStyle(EMPRESA_CLIENTE); ?>
    <!--/CLASSE ASSOCIADA AO IDIOMA-->
        
    <!-- Desenvolvido por <?= $creditos . " - " . $siteCreditos ?> -->

<!--     <script>
      $(document).ready(function () {
        var altura = document.getElementById('scrollheader').offsetHeight;


        window.onscroll = function () {
          Scroll()
        };
        function Scroll() {
          if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
            document.getElementById("scrollheader").className = "topofixo";
            document.getElementById("header-block").style.display = "block";
            document.getElementById("header-block").style.height = altura + "px";

          } else {
            document.getElementById("scrollheader").className = "";
            document.getElementById("header-block").style.display = "none";
          }
        }
      });
    </script> -->